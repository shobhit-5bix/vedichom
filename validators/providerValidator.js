const validator = require("./validator.js");
const {
  GENDER_TYPES,
  RESPONSE_TIME,
  SESSION_PREFERENCE,
  LESSON_PREFERENCE,
} = require("../constants.js");
const mongoose = require("mongoose");

const providerApiPathModels = {
  "/register": {
    firstName: { type: String, required: true },
    lastName: { type: String },
    email: { type: String, required: true },
    password: { type: String, required: true },
    mobileNo: { type: Number },
  },
  "/login": {
    email: { type: String },
    password: { type: String },
  },
  "/logout": {},
  "/verify-email": {
    otp: { type: String },
    email: { type: String },
  },
  "/resend-otp": {},

  "/forget-password": {
    email: { type: String, required: true },
  },
  "/reset-pass-otp-verify": {
    email: { type: String, required: true},
    otp: { type: String,  required: true },
  },
  "/reset-password": {
    password: { type: String,  required: true },
  },
  "/delete-account": {},

  // TODO : add fields in this ->/
  "/create-update-ad": {
    services: { type: Array },
    vedicSubject: { type: String },
    skillSet: { type: Array },
    title: { type: String },
    aboutClass: { type: String },
    aboutYou: { type: String },
    phoneNo: { type: Number },
    language: { type: Array },
    location: { type: String },
    modeOfClass: { type: String, enum: Object.values(SESSION_PREFERENCE) },
    lesson: { type: String, enum: Object.values(LESSON_PREFERENCE) }, //Private, group or any
    hourlyRate: { type: Number },
    profileImage: { type: String },
    profileVideo: { type: String },
    packages: { type: Object },
    demoVideo: { type: String },
    youtubeLink: { type: String },
  },
  "/upload-ad-media": {
    profileImageUrl: { type: String },
    profileVideoUrl: { type: String },
    demoVideoUrl: { type: String },
  },
  "/accept-delete-request": {
    requestId: { type: String },
    action: { type: String },
  },
  "/get-provider-account-details": {},
  "/update-provider-account-details": {
    firstName: { type: String },
    lastName: { type: String },
    gender: { type: String, enum: Object.values(GENDER_TYPES) },
    dob: { type: Date },
    mobileNo: { type: Number },
    landlineNo: { type: Number },
    skypeId: { type: String },
    TelegramId: { type: String },
    yearsOfExp: { type: Number },
    skills: { type: Array },
    postalAddress: { type: String },
    bankDetails: { type: String },
    responseTime: { type: String, enum: RESPONSE_TIME },
  },
  "/change-pass-provider-account": {
    password: { type: String },
    newPassword: { type: String },
  },
  "/get-provider-requests": {},
  "/get-rejected-requests": {},
  "/sort-requests": {},
  "/search-sort-filter-requests": {},
  "/get-provider-ad": {},
  "/update-ad-status": {},
  "/get-provider-reviews": {},
  "/count-reviews": {},
  "/send-review-reply": {
    reviewId: { type: String },
    message: { type: String },
  },
  "/calculate-avg-rating": {},


  // account api
  "/upload-docs": {
    certiUrl: { type: Array },
    credsUrl: { type: String },
  },
  "/upload-profile-pic": {
    profilePic: { type: String },
  },
  "/delete-advertisement": {
    adId: { type: String, required: true }
  },
  "/ask-ad-faq's": {
    adId: { type: String },
    question: { type: String }
  },
  "/get-ad-faq's": {
    adId: { type: String },
    pageNo: { type: Number },
    pageLimit: { type: Number },
  },
  "/reply-update-ad-faq's": {
    faqId: { type: String, required: true },
    answer: { type: String, required: true },
  },
  "/get-exp-status": {},

  // google auth 
  "/auth/google": {},
  "/auth/google/callback": {}
};

function providerValidator(req, res, next) {
  if (!providerApiPathModels[req.path]) {
    console.error(req.path, ": Missing Parameters");
    return res.status(400).json({ message: "No validator for this path" });
  }

  validator(req, res, next, providerApiPathModels[req.path]);
}

module.exports = { providerValidator };
