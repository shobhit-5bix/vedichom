const validator = require("./validator.js");

const notificationApiPathModels = {
  "/get-notifications": {
    pageNo: { type: Number },
    pageLimit: { type: Number },
  },
  "/close-notification": {
    notificationId: { type: String },
    closeAll: { type: Boolean },
  },
  "/get-notifications-from-cache": {},
  "/del-notifications-from-cache": {},
};

function notificationValidator(req, res, next) {
  if (!notificationApiPathModels[req.path]) {
    console.error(req.path, ": Missing Parameters");
    return res.status(400).json({ message: "No validator for this path" });
  }

  validator(req, res, next, notificationApiPathModels[req.path]);
}

module.exports = { notificationValidator };
