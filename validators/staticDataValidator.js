const validator = require("./validator.js");

const staticDataApiPathModels = {
  "/get-vedic-subject": {},
  "/get-sub-suggestion": {
    subject: {type: String}
  },
  "/get-languages": {},
  "/get-faq": {},
  "/get-current-location": {
    lat: { type: String, required: true },
    lon: { type: String, required: true },
  },
'/get-reverse-geocoding':{
  longitude: {type: String, required: true},
  latitude: {type: String, required: true}
},
'/get-location-suggestion':{
  search: {type: String, required: true}
},
};

function staticDataValidator(req, res, next) {
  if (!staticDataApiPathModels[req.path]) {
    console.error(req.path, ": Missing Parameters");
    return res.status(400).json({ message: "No validator for this path" });
  }

  validator(req, res, next, staticDataApiPathModels[req.path]);
}

module.exports = { staticDataValidator };
