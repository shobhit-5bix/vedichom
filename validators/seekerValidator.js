const validator = require("./validator.js");
const { USER_TYPES, GENDER_TYPES, STATUS } = require("../constants.js");
const { Schema } = require("mongoose");

const seekerApiPathModels = {
  "/register": {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    email: { type: String, required: true },
    password: { type: String, required: true },
    mobileNo: { type: Number, required: true },
  },
  "/login": {
    email: { type: String },
    password: { type: String },
  },
  "/logout": {},
  "/verify-email": {
    email: { type: String },
    otp: { type: String }
  },
  "/resend-otp": {},
  "/forget-password": {
    email: { type: String, required: true },
  },
  "/reset-pass-otp-verify": {
    email: { type: String, required: true},
    otp: { type: String,  required: true },
  },
  "/reset-password": {
    password: { type: String,  required: true },
  },
  "/delete-account": {},
  "/search-sort-providers": {
    vedicSubject: { type: String },
    services: { type: String },
    modeOfClass: { type: String },
    sortBy: { type: String, enum: ["price"] },
    sortOrder: { type: String, enum: ["ascending", "descending"] },
  },
  "/get-provider-ad-details-reviews": {},
  "/make-fav-ad": {},
  "/get-fav-ads": {},
  "/get-profile": {},
  "/change-password": {
    password: { type: String },
    newPassword: { type: String },
  },
  "/update-profile": {
    firstName: { type: String },
    lastName: { type: String },
    gender: { type: String, enum: Object.values(GENDER_TYPES) },
    dob: { type: Date },
    mobileNo: { type: Number },
    landlineNo: { type: Number },
    skypeId: { type: String },
    TelegramId: { type: String },
    certification: { type: Array },
    credentials: { type: String },
    postalAddress: { type: String },
    bankDetails: { type: String },

    imageUrl: { type: String },
  },
  "/create-request": {
    adId: { type: String, required: true },
    sessionPreference: { type: String, },
    sessionForWhom: { type: String },
    requestMessage: { type: String },
  },
  "/delete-request": {
    requestId: { type: String, required: true },
  },

  "/report-ad": {
    adId: {type: String, required: true},
    category: {type: String, required: true},
    message: {type: String, required: true},
  },
  //seeker dashboard routes ----------

  "/get-request": {},
  "/get-reviews": {},
  "/get-list-of-accepted-requests": {},
  "/create-review": {
    providerId: { type: String, required: true },
    rating: { type: String, required: true },
    message: { type: String },
  },
  "/get-rated-unrated-reviews": {},
  "/sort-request": {},
  "/search-sort-filter-request": {},
  "/chat-write": {
    to: { type: String },
    text: { type: String },
  },
  "/chat-read": {},

  // payment apis
  "/payment/checkout": {
    amount: { type: String },
  },
  "/payment/payment-verify": {
    amount: { type: String },
  },
  "/payment/get-key": {},
  "/payment/get-payment": {},

  // account
  "/upload-docs": {},
  "/auth/google": {},
  "/auth/google/callback": {},

  // help & support
  "/help-and-support": {
    category: {type: String},
    message: {type: String},
  },
};

function seekerValidator(req, res, next) {
  //  const excludedPaths = ["/login", "/register"]
  //  console.log(req)
  // if (req.role != USER_TYPES.SEEKER && !excludedPaths.includes(req.path) )
  //   return res
  //     .status(401)
  //     .json({ message: "You are not authorised to access this route" });

  if (!seekerApiPathModels[req.path]) {
    console.error(req.path, ": Missing Parameters");
    return res.status(400).json({ message: "No validator for this path" });
  }

  validator(req, res, next, seekerApiPathModels[req.path]);
}

module.exports = { seekerValidator };
