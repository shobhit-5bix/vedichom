const multer = require("multer");

const express = require('express');
const { verifyToken, verifyTokenForLogout } = require('../auth/authUtils.js');
const seekerServices = require('../services/seekerServices.js');
const seekerDashboardServices = require('../services/seekerDashboardServices.js');
const chatServices = require('../services/chatServices.js')
const paymentServices = require('../services/paymentServices.js')
const { seekerValidator } = require('../validators/seekerValidator.js');
const router = express.Router();
const passport = require('passport');

router.use(seekerValidator);


const upload = multer({ storage: multer.memoryStorage() });
const cpUpload = upload.single("profilePic");
const multiUpload = upload.fields([
    { name: 'certificate', maxCount: 3 },
    { name: 'credential', maxCount: 1 },
]);

//BASIC APIS
router.post('/register', seekerServices.registerUser);
router.post('/login', seekerServices.loginUser);
router.get('/logout', [verifyTokenForLogout], seekerServices.logoutUser);
router.post('/verify-email', seekerServices.verifyEmail);
router.get('/resend-otp', seekerServices.resendOtp);
router.post('/forget-password', seekerServices.forgetPassword);
router.post('/reset-pass-otp-verify', seekerServices.resetPasswordOtpVerify);
router.post('/reset-password', [verifyToken], seekerServices.resetPassword);
router.post('/change-password', [verifyToken], seekerServices.changePassword);
router.post('/delete-account', [verifyToken], seekerServices.deleteAccount);

//---------------------------------------------------------------

router.get('/search-sort-providers', [verifyToken], seekerServices.searchSortProviders);
router.get('/get-provider-ad-details-reviews', [verifyToken], seekerServices.getProviderAdAndReviews);
router.get('/get-profile', [verifyToken], seekerServices.getProfile);
router.get('/make-fav-ad', [verifyToken], seekerServices.makeAdFavourite);
router.get('/get-fav-ads', [verifyToken], seekerServices.getFavAds);
router.post('/update-profile', [verifyToken], cpUpload, seekerServices.updateSeekerAccountDetails);
router.post('/create-request', [verifyToken], seekerServices.createRequest);
router.post('/delete-request', [verifyToken], seekerServices.deleteRequest);

router.post('/report-ad', [verifyToken], seekerServices.reportAdvertisement);

//SEEKER DASHBOARD APIs-------------------------------------------

// router.get('/get-user-profile', [verifyToken], seekerDashboardServices.getSeekerAccountDetails);
router.get('/get-request', [verifyToken], seekerDashboardServices.getSeekersRequest);
router.get('/get-reviews', [verifyToken], seekerDashboardServices.getSeekersReviews);
router.get('/get-list-of-accepted-requests', [verifyToken], seekerDashboardServices.getListOfAcceptedRequests);
router.post('/create-review', [verifyToken], seekerDashboardServices.createReview);
router.get('/get-rated-unrated-reviews', [verifyToken], seekerDashboardServices.getRatedUnratedReviews);

router.get('/sort-request', [verifyToken], seekerDashboardServices.sortRequest);
router.get('/search-sort-filter-request', [verifyToken], seekerDashboardServices.searchSortFilterSeekersRequest);


// Firebase chat apis
router.post('/chat-write', [verifyToken], chatServices.createFirebaseMessage);
router.get('/chat-read', [verifyToken], chatServices.readFirebaseMessage);

// Payment apis
router.post('/payment/checkout', [verifyToken], paymentServices.checkout);
router.post('/payment/payment-verify', [verifyToken], paymentServices.paymentVerifation);
router.get('/payment/get-key', [verifyToken], paymentServices.getKey);
router.get('/payment/get-payment', [verifyToken], paymentServices.getPayment);

// Account apis
router.post('/upload-docs', [verifyToken], multiUpload, seekerDashboardServices.uploadSeekersDocuments);


// Google Auth apis
router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

router.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/' }), seekerServices.registerSeeker);

// Help & Support
router.post('/help-and-support', [verifyToken], seekerDashboardServices.sendHelpAndSupportMessage);


module.exports = router;
