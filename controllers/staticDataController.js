const express = require('express');
const staticDataServices = require('../services/staticDataServices.js');
const {staticDataValidator} = require('../validators/staticDataValidator.js');
const router = express.Router();

router.use(staticDataValidator);


router.get('/get-vedic-subject', staticDataServices.vedicSubjectsData);
router.get('/get-sub-suggestion', staticDataServices.vedicSubSuggestion);
router.get('/get-languages', staticDataServices.languageData);
router.get('/get-faq', staticDataServices.getFaqs);

router.get('/get-current-location', staticDataServices.getGeoLocation);
router.get('/get-reverse-geocoding', staticDataServices.getReverseGeocoding);
router.get('/get-location-suggestion', staticDataServices.getLocationSuggestion);


module.exports = router;
