const express = require("express");
const router = express.Router({ caseSensitive: true });

const { verifyToken } = require("../auth/authUtils.js");
const notificationServices = require("../services/notificationService.js");
const { notificationValidator } = require("../validators/notificationValidator.js");

router.use(notificationValidator);

router.get("/get-notifications-from-cache", [verifyToken], notificationServices.getNotificationFromCache);
router.get("/del-notifications-from-cache", [verifyToken], notificationServices.deleteNotificationInCache);
router.get("/get-notifications", [verifyToken], notificationServices.getNotifications);
router.post("/close-notification", [verifyToken], notificationServices.closeNotification);

module.exports = router;
