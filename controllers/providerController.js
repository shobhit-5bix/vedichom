const express = require("express");
const { verifyToken, verifyTokenForLogout } = require("../auth/authUtils.js");
const providerServices = require("../services/providerServices.js");
const providerDashboardServices = require("../services/providerDashboardServices.js");
const { providerValidator } = require("../validators/providerValidator.js");
const multer = require("multer");
const router = express.Router();
const passport = require('passport');
router.use(providerValidator);

const upload = multer({ storage: multer.memoryStorage() });
const cpUpload = upload.single("profilePic");
const multiUpload = upload.fields([
  { name: "certificate", maxCount: 3 },
  { name: "credential", maxCount: 1 },
]);
const mediaUpload = upload.fields([
  { name: "profileImage", maxCount: 1 },
  { name: "profileVideo", maxCount: 1 },
  { name: "demoVideo", maxCount: 1 },
]);

router.post("/register", providerServices.registerUser);
router.post("/login", providerServices.loginUser);
router.get("/logout", [verifyTokenForLogout], providerServices.logoutUser);
router.post("/verify-email", providerServices.verifyEmail);
router.get("/resend-otp", providerServices.resendOtp);
router.post("/forget-password", providerServices.forgetPassword);
router.post("/reset-pass-otp-verify", providerServices.resetPasswordOtpVerify);
router.post("/reset-password", [verifyToken], providerServices.resetPassword);
router.post("/delete-account", [verifyToken], providerServices.deleteAccount);
router.post(
  "/create-update-ad",
  [verifyToken],
  providerServices.createUpdateAd
);
router.post(
  "/accept-delete-request",
  [verifyToken],
  providerServices.acceptOrRejectRequest
);
router.delete("/delete-advertisement", [verifyToken], providerServices.deleteAdvertisement);

//---------------------------------| Provider Dashboard APIs |-----------------------------

router.get(
  "/get-provider-account-details",
  [verifyToken],
  providerDashboardServices.getProviderAccountDetails
);
router.post(
  "/update-provider-account-details",
  [verifyToken],
  providerDashboardServices.updateProviderAccountDetails
);
router.post(
  "/change-pass-provider-account",
  [verifyToken],
  providerDashboardServices.changePasswordProviderAccount
);
router.get(
  "/get-provider-requests",
  [verifyToken],
  providerDashboardServices.getProviderRequests
);
router.get(
  "/get-rejected-requests",
  [verifyToken],
  providerDashboardServices.getRejectedRequests
);
router.get(
  "/sort-requests",
  [verifyToken],
  providerDashboardServices.sortRequest
);
router.get(
  "/search-sort-filter-requests",
  [verifyToken],
  providerDashboardServices.searchSortFilterProvidersRequest
);
// ad apis
router.get(
  "/get-provider-ad",
  [verifyToken],
  providerDashboardServices.getProviderAdvertisements
);
router.post(
  "/update-ad-status",
  [verifyToken],
  providerDashboardServices.updateAdvertisementStatus
);

// --review apis
router.get(
  "/get-provider-reviews",
  [verifyToken],
  providerDashboardServices.getProvidersReview
);

router.post(
  "/send-review-reply",
  [verifyToken],
  providerDashboardServices.sendReviewReply
);

router.get(
  "/calculate-avg-rating",
  [verifyToken],
  providerDashboardServices.calculateProvidersAvgRating
);

router.post(
  "/upload-ad-media",
  [verifyToken],
  mediaUpload,
  providerDashboardServices.uploadAdvertisementMedia
);

// account api
router.post(
  "/upload-docs",
  [verifyToken],
  multiUpload,
  providerDashboardServices.uploadProviderDocuments
);

router.post(
  "/upload-profile-pic",
  [verifyToken],
  cpUpload,
  providerDashboardServices.updateProviderProfilePicture
);

router.post(
  "/ask-ad-faq's",
  providerDashboardServices.askFaq
);

router.get(
  "/get-ad-faq's",
  providerDashboardServices.getFaq
);

router.post(
  "/reply-update-ad-faq's",
  [verifyToken],
  providerDashboardServices.replyOrUpdateFaq
);

router.get(
  "/get-exp-status",
  [verifyToken],
  providerDashboardServices.getProviderExpStatus
);

// social logins
router.get('/auth/google', passport.authenticate('google', { scope: ['profile', 'email'] }));

router.get('/auth/google/callback', passport.authenticate('google', { failureRedirect: '/' }), providerServices.registerProvider);

// Help & Support
router.post('/help-and-support', [verifyToken], providerDashboardServices.sendHelpAndSupportMessage);

module.exports = router;

// TODO : validator not working properly
