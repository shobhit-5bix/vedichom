const redis = require("redis");

const redisClient = redis
  .createClient({
    host: "127.0.0.1:6379",
    retry_max_delay: 1000,
    max_attempts: 10,
    retry_unfulfilled_commands: true,
    // password: process.env.REDIS_KEY,
  })


const log = (type, fn) =>
  fn
    ? () => {
        if (process.env.NODE_ENV !== "test")
          console.info(`Redis connection: ${type}`);
      }
    : console.info(`Redis connection: ${type}`);

// Handle events
redisClient.connect();
redisClient.on("connect", log("connect", true));
redisClient.on("ready", log("ready", true));
redisClient.on("reconnecting", log("reconnecting", true));
redisClient.on("error", log("error", true));
redisClient.on("end", log("end", true));

// Authentication (uncomment if authentication is required)
// redisClient.auth(process.env.REDIS_KEY || "");

/**
 * Promisified version of `redis.get(key)`
 * @param {string} key - The key to search in Redis
 * @returns {Promise<string>} - Promise with `reply` in resolution and `error` in rejection
 */
// const getAsync = (key) => {
//   return new Promise((resolve, reject) => {
//     redisClient.get(key, (err, reply) => {
//       if (err) return reject(err);
//       return resolve(reply);
//     });
//   });
// };

const getAsync = (key) => {
  console.log(`Attempting to retrieve value for key: ${key}`);
  return new Promise((resolve, reject) => {
    redisClient.get(key, (err, reply) => {
      if (err) {
        console.error(`Error in getAsync for key '${key}':`, err);
        return reject(err);
      }
      console.log(`Successfully retrieved value for key '${key}': ${reply}`);
      return resolve(reply);
    });
  });
};



/**
 * Promisified version of `redis.exists(key)`
 * @param {string} key - The key to search in Redis
 * @returns {Promise<boolean>} - Promise with `reply` in resolution and `error` in rejection
 */
const existsAsync = (key) => {
  return new Promise((resolve, reject) => {
    redisClient.exists(key, (err, reply) => {
      if (err) return reject(err);
      return resolve(reply);
    });
  });
};

/**
 * Promisified version of `redis.set(key)`
 * @param {string} key - The key to set in Redis
 * @param {string} value - The value to be set in Redis corresponding to the key
 * @returns {Promise<string>} - Promise with `reply` in resolution
 */
const setAsync = (key, value) => {
  return new Promise((resolve, reject) => {
    redisClient.set(key, value, (reply) => {
      return resolve(reply);
    });
  });
};

/**
 * Promisified version of `redis.del(key)`
 * @param {string} key - The key to delete from Redis
 * @returns {Promise<number>} - Promise with `reply` in resolution
 */
const delAsync = (key) => {
  return new Promise((resolve, reject) => {
    redisClient.del(key, (reply) => {
      return resolve(reply);
    });
  });
};

module.exports = {
  redisClient,
  getAsync,
  existsAsync,
  setAsync,
  delAsync,
};
