const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
// const { createClient } = require("redis");
const { RESPONSE_STATUS, RESPONSE_MESSAGES } = require("../constants");

const JWT_SECRET = "23420349sdfsfs";

// const client = createClient();
// client.on("error", (err) => console.log("Redis Client Error", err));
// client.connect().then(() => {
//   console.log("client is connected ");
// });

const ByPass = [
  "/login",
  "/search-sort-providers",
  "/get-provider-ad-details-reviews",
];

async function verifyToken(req, res, next) {
  try {
    if (ByPass.includes(req.path) && !req.headers["authorization"])
      return next();
    token = req.headers["authorization"];
    if (!token) {
      return res
        .status(RESPONSE_STATUS.UNAUTHORIZED)
        .json({ message: RESPONSE_MESSAGES.TOKEN_NOT_FOUND });
    }
    token = token.split(" ");
    token = token.length > 1 ? token[1] : token[0];

    jwt.verify(
      token,
      process.env.JWT_SECRET || JWT_SECRET,
      async (err, decoded) => {
        if (err) {
          return res.status(RESPONSE_STATUS.UNAUTHORIZED).json({
            message: RESPONSE_MESSAGES.TOKEN_SESSION,
            detail: "Error in token verification, in verifyToken middleware",
          });
        }
        req.userId = decoded.userId;
        req.role = decoded.role;
        // Check if token is stored in redis

        // const redisToken = await client.get(`${req.userId}`);

        //   if (redisToken != token) {
        //     return res
        //       .status(RESPONSE_STATUS.UNAUTHORIZED)
        //       .json({ message: RESPONSE_MESSAGES.TOKEN_SESSION });
        //   }
        next();
      }
    );
  } catch (error) {
    console.error("verify token function :", error);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
}

async function generateToken(params) {
  let response = {};
  try {
    let userId = params.userId || params._id;
    let email = params.email || params.req.body.email; //TODO: params.req.body.email will not work;
    let token = jwt.sign(
      { userId, email, role: params.role },
      process.env.JWT_SECRET,
      {
        expiresIn: "24h",
      }
    ); // expires in 24 hours
    // await client.set(`${userId}`, `${token}`, {
    //   EX: 60 * 60 * 24,
    // });
    return token;
  } catch (error) {
    console.error("Generating Token", "Error Occurred !!!", error);
    return (response.error = error);
  }
}

async function logoutToken(req) {
  try {
    // await client.del(req.userId);
    delete req.userId;
    return true;
  } catch (error) {
    console.error("Error while logging out :", error);
    return false;
  }
}

async function verifyTokenForLogout(req, res, next) {
  try {
    
    token = req.headers["authorization"];
    if (!token) {
      return res
        .status(RESPONSE_STATUS.UNAUTHORIZED)
        .json({ message: RESPONSE_MESSAGES.TOKEN_NOT_FOUND });
    }
    token = token.split(" ");
    token = token.length > 1 ? token[1] : token[0];

    jwt.verify(
      token,
      process.env.JWT_SECRET || JWT_SECRET,
      async (err, decoded) => {
        if (err) {
          return res.status(RESPONSE_STATUS.SUCCESS).json({
            message: RESPONSE_MESSAGES.TOKEN_SESSION,
            detail: "Session expired, user has already been logout!",
          });
        }
        req.userId = decoded.userId;
        req.role = decoded.role;
        // Check if token is stored in redis
        next();
      }
    );
  } catch (error) {
    console.error("Error in verifyTokenForLogout middleware :", error);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
}

exports.verifyToken = verifyToken;
exports.generateToken = generateToken;
exports.logoutToken = logoutToken;
exports.verifyTokenForLogout = verifyTokenForLogout;
