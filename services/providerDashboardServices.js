const Providers = require("../models/providers.js");
const Requests = require("../models/request.js");
const Advertisements = require("../models/advertisements.js");
const Reviews = require("../models/review.js");
const { fileUpload } = require("../services/s3Services.js");
const {
  RESPONSE_STATUS,
  RESPONSE_MESSAGES,
  STATUS,
  NUMERIC_CONSTANTS,
  IMAGES_MIMETYPE,
  VIDEO_MIMETYPE,
  REQUEST_STATUS,
  USER_TYPES,
} = require("../constants.js");
const bcrypt = require("bcrypt");
const FAQS = require("../models/faq.js");
const { isEmailValid, capitalizedWord } = require("../commonFunctions.js");
const { default: mongoose } = require("mongoose");
const {createPushNotifications} = require("../services/notificationService.js")

//----------------------------------------Dashboard apis----------------------------------------

const getProviderAccountDetails = async (req, res) => {
  try {
    const account = await Providers.findOne(
      { _id: req.userId },
      { password: 0, createdAt: 0, updatedAt: 0 }
    ).lean();
    if (!account) {
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json(RESPONSE_MESSAGES.NOT_FOUND);
    }
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: account });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getProviderRequests = async (req, res) => {
  try {
    const [usersAd, provider] = await Promise.all([
      Advertisements.find({ providerId: req.userId }, { _id: 1 }),
      Providers.findById(req.userId, { isPremium: 1 }).lean(),
    ]);
    const arrayOfId = usersAd.map((obj) => obj._id);

    let allRequests;
    // provider.isPremium = false;
    if (provider.isPremium) {
      allRequests = await Requests.find({
        adId: { $in: arrayOfId },
        // $or: [
        //   { requestStatus: REQUEST_STATUS.PENDING },
        //   { requestStatus: REQUEST_STATUS.ACCEPT },
        // ],
      }).populate({
        path: "seekerId adId",
        select: "firstName postalAddress vedicSubject profilePic",
      });
    } else {
      allRequests = await Requests.find({
        adId: { $in: arrayOfId },
        // $or: [
        //   { requestStatus: REQUEST_STATUS.PENDING },
        //   { requestStatus: REQUEST_STATUS.ACCEPT },
        // ],
      })
        .limit(2)
        .populate({
          path: "seekerId adId",
          select: "firstName postalAddress vedicSubject profilePic",
        });
    }

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: allRequests });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

// const rejectRequests = async (req, res) => {
//   try {
//     const { requestId } = req.body;

//     const requestToDel = await Requests.findOne(
//       {
//         _id: requestId,
//         requestStatus: REQUEST_STATUS.PENDING,
//       },
//       { _id: 1 }
//     );
//     if (!requestToDel) {
//       return res
//         .status(RESPONSE_STATUS.BAD_REQUEST)
//         .json({ message: "Request is not Valid or does not Exist!" });
//     }

//     requestToDel.requestStatus = REQUEST_STATUS.DELETE;
//     requestToDel.save();

//     return res
//       .status(RESPONSE_STATUS.SUCCESS)
//       .json({ message: "Request has been Successfully Rejected!" });
//   } catch (error) {
//     console.error(error);
//     return res
//       .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
//       .json({ message: error.message || error.stack });
//   }
// };

const getRejectedRequests = async (req, res) => {
  try {
    const usersAd = await Advertisements.find(
      { providerId: req.userId },
      "_id"
    );
    const arrayOfId = usersAd.map((obj) => obj._id);

    const allRequests = await Requests.find({
      adId: { $in: arrayOfId },
      requestStatus: REQUEST_STATUS.DELETE,
    }).populate({
      path: "seekerId adId",
      select: "firstName postalAddress vedicSubject profilePic",
    });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: allRequests });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getProviderAdvertisements = async (req, res) => {
  try {
    const [totalReviews, allAdvertisements] = await Promise.all([
      Reviews.count({ providerId: req.userId }),
      Advertisements.find({
        providerId: req.userId,
        status: { $ne: STATUS.DELETED },
      }).populate({ path: "providerId", select: "-password" }),
    ]);
    // TODO: I have used a function here to update skills.
    await updateProviderSkills(req.userId);

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.SUCCESS,
      data: { totalReviews, allAdvertisements },
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

// Provider functions =======================================================
const updateProviderSkills = async (userId) => {
  try {
    const [user, providersAds] = await Promise.all([
      Providers.findById(userId),
      Advertisements.find({ providerId: userId }).distinct("vedicSubject"),
    ]);

    for (const subject in providersAds) {
      if (!user.skills.includes(providersAds[subject])) {
        user.skills.push(providersAds[subject]);
      }
    }
    await user.save();
    return 1;
  } catch (error) {
    throw new Error("Error in updateProviderSkills function");
  }
};

const updateAdvertisementStatus = async (req, res) => {
  try {
    const { adId } = req.body;
    const ad = await Advertisements.findOne({
      _id: adId,
      providerId: req.userId,
    });

    if (!ad) {
      return res
        .status(RESPONSE_STATUS.UNAUTHORIZED)
        .json({ message: RESPONSE_MESSAGES.UNAUTHORIZED });
    }

    ad.status = ad.status == STATUS.ACTIVE ? STATUS.CLOSED : STATUS.ACTIVE;
    ad.save();

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.SUCCESS,
      data: `Advertisement status: ${ad.status}`,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const uploadAdvertisementMedia = async (req, res) => {
  try {
    const {
      body: { profileImageUrl, profileVideoUrl, demoVideoUrl },
      files,
      query: { adId },
    } = req;
    const ad = await Advertisements.findOne({
      _id: adId,
      providerId: req.userId,
    }).select("profileImage profileVideo demoVideo");

    if (!ad) {
      return res
        .status(RESPONSE_STATUS.UNAUTHORIZED)
        .json({ message: RESPONSE_MESSAGES.UNAUTHORIZED });
    }

    if (profileImageUrl || profileVideoUrl || demoVideoUrl) {
      if (profileImageUrl) ad.profileImage = profileImageUrl;
      if (profileVideoUrl) ad.profileVideo = profileVideoUrl;
      if (demoVideoUrl) ad.demoVideo = demoVideoUrl;

      ad.save();
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: RESPONSE_MESSAGES.SUCCESS, data: ad });
    } else {
      let uploadedProfileImage;
      let uploadedProfileVideo;
      let uploadedDemoVideo;
      if (files.profileImage) {
        const profileImage = files.profileImage[0];
        if (profileImage.size > NUMERIC_CONSTANTS.PROFILE_PIC_SIZE) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Image size should not be greater than 2MB!",
          });
        }
        if (!IMAGES_MIMETYPE.includes(profileImage.mimetype)) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message:
              "Only Images with JPG, JPEG, PNG, X-PNG, BMP formats can be uploaded!",
          });
        }

        uploadedProfileImage = await fileUpload(
          "vedicHom/advertisements",
          `${req.userId}${profileImage.originalname}`,
          profileImage.buffer
        );
        ad.profileImage = uploadedProfileImage;
      }
      if (files.profileVideo) {
        const profileVideo = files.profileVideo[0];
        if (profileVideo.size > NUMERIC_CONSTANTS.PROFILE_VIDEO_SIZE) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Video size should not be greater than 20MB!",
          });
        }
        if (!VIDEO_MIMETYPE.includes(profileVideo.mimetype)) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message:
              "Only videos with mp4, mpeg, mkv, ogg, webm, quicktime formats can be uploaded!",
          });
        }

        uploadedProfileVideo = await fileUpload(
          "vedicHom/advertisements",
          `${req.userId}${profileVideo.originalname}`,
          profileVideo.buffer
        );
        ad.profileVideo = uploadedProfileVideo;
      }
      if (files.demoVideo) {
        const demoVideo = files.demoVideo[0];
        if (demoVideo.size > NUMERIC_CONSTANTS.PROFILE_VIDEO_SIZE) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Video size should not be greater than 20MB!",
          });
        }
        if (!VIDEO_MIMETYPE.includes(demoVideo.mimetype)) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message:
              "Only videos with mp4, mpeg, mkv, ogg, webm, quicktime formats can be uploaded!",
          });
        }

        uploadedDemoVideo = await fileUpload(
          "vedicHom/advertisements",
          `${req.userId}${demoVideo.originalname}`,
          demoVideo.buffer
        );
        ad.demoVideo = uploadedDemoVideo;
      }
    }

    ad.save();
    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.SUCCESS,
      data: ad,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const updateProviderAccountDetails = async (req, res) => {
  try {
    const user = await Providers.findOne({ _id: req.userId }).select(
      "-password"
    );
    if (!user) {
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json({ message: RESPONSE_MESSAGES.USER_NOT_FOUND });
    }
    if (req.body.email) {
      const isValid = isEmailValid(req.body.email);
      if (!isValid)
        return res
          .status(RESPONSE_STATUS.BAD_REQUEST)
          .json({ message: "Please enter a valid email address." });
      user.email = req.body.email;
    }
    if (req.body.firstName) {
      user.firstName = capitalizedWord(req.body.firstName);
    }

    if (req.body.lastName) {
      user.lastName = capitalizedWord(req.body.lastName);
    }

    if (req.body.mobileNo) user.mobileNo = req.body.mobileNo;
    if (req.body.gender) user.gender = req.body.gender;
    if (req.body.landlineNo) user.landlineNo = req.body.landlineNo;
    if (req.body.dob) user.dob = req.body.dob;
    if (req.body.postalAddress) user.postalAddress = req.body.postalAddress;
    if (req.body.skypeId) user.skypeId = req.body.skypeId;
    if (req.body.TelegramId) user.TelegramId = req.body.TelegramId;
    if (req.body.yearsOfExp) user.yearsOfExp = req.body.yearsOfExp;
    if (req.body.skills) user.skills = req.body.skills;
    if (req.body.bankDetails) user.bankDetails = req.body.bankDetails;
    if (req.body.responseTime) user.responseTime = req.body.responseTime;

    await user.save();
    delete user.password;

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: user });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const updateProviderProfilePicture = async (req, res) => {
  try {
    const {
      body: { profilePicUrl },
      file,
    } = req;

    const user = await Providers.findById(req.userId).select(
      "firstName email profilePic"
    );

    if (profilePicUrl) {
      user.profilePic = profilePicUrl;
      user.save();
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Profile Pic Uploaded Successfully", data: user });
    } else {
      if (file) {
        if (file.size > NUMERIC_CONSTANTS.PROFILE_PIC_SIZE) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Image size should not be greater than 2MB!",
          });
        }
        if (!IMAGES_MIMETYPE.includes(file.mimetype)) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Only JPG, JPEG, PNG, X-PNG, BMP formats can be uploaded!",
          });
        }

        const uploadedPic = await fileUpload(
          "vedicHom/profilePic",
          `${req.userId}${file.originalname}`,
          file.buffer
        );
        user.profilePic = uploadedPic;
        user.save();
        return res
          .status(RESPONSE_STATUS.SUCCESS)
          .json({ message: "Image uploaded Successfully", data: user });
      }
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Please Upload an Image or Image Url" });
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const changePasswordProviderAccount = async (req, res) => {
  try {
    const { password, newPassword } = req.body;
    const passChangeOptions = { ...req.body };

    const user = await Providers.findById(req.userId).select("-passoword");

    if (bcrypt.compareSync(passChangeOptions.password, user.password)) {
      const hashedPassword = bcrypt.hashSync(passChangeOptions.newPassword, 8);
      user.password = hashedPassword;
    } else {
      return res
        .status(RESPONSE_STATUS.FORBIDDEN)
        .json({ message: RESPONSE_MESSAGES.PASS_MISMATCH });
    }
    await user.save();
    delete user.password;

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Password Successfully Changed" });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const uploadProviderDocuments = async (req, res) => {
  try {
    const {
      body: { certiUrl, credsUrl },
      files,
    } = req;

    const user = await Providers.findOne({ _id: req.userId }).select(
      "certification credentials firstName"
    );

    if (certiUrl || credsUrl) {
      if (certiUrl) user.certification = certiUrl;
      if (credsUrl) user.credentials = credsUrl;
      user.save();

      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Image link uploaded successfully!", data: user });
    }

    if (files.certificate) {
      for (const element of files.certificate) {
        if (element.size > NUMERIC_CONSTANTS.DOCUMENT_SIZE) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Documents size should not be greater than 10MB!",
          });
        }
        if (element.mimetype != "application/pdf") {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Only pdf format can be uploaded!",
          });
        }
      }
    }

    if (files.credential) {
      const credential = files.credential[0];
      if (credential.size > NUMERIC_CONSTANTS.DOCUMENT_SIZE) {
        return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
          message: "Documents size should not be greater than 10MB!",
        });
      }
      if (credential.mimetype != "application/pdf") {
        return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
          message: "Only pdf format can be uploaded!",
        });
      }
    }

    let uploadedCerti = [];
    if (files.certificate) {
      uploadedCerti = await Promise.all(
        files.certificate.map(async (element) => {
          return await fileUpload(
            "vedicHom/documents",
            `${req.userId}${element.originalname}`,
            element.buffer
          );
        })
      );
    }

    let uploadedCreds;
    if (files.credential) {
      uploadedCreds = await fileUpload(
        "vedicHom/documents",
        `${req.userId}${files.credential[0].originalname}`,
        files.credential[0].buffer
      );
    }

    if (uploadedCerti.length) user.certification = uploadedCerti;
    if (uploadedCreds) user.credentials = uploadedCreds;

    await user.save();

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: user });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getProviderExpStatus = async (req, res) => {
  try {
    // have two recomendation
    // upload ad video
    // upload profile image
    // create an ad
    // write about yourself
    // write subject description in ad
    const result = [
      {
        name: "Upload Profile Image",
        value: false,
      },
      { name: "Create an Ad", value: false},
      {
        name: "Write about Yourself",
        value: false,
      },
      {
        name: "Write subject description in your Ad",
        value: false,
      },
      { name: "Upload Ad Video", value: false,  },
    ];

    const [user, yourAds] = await Promise.all([
      Providers.findById(req.userId).lean(),
      Advertisements.find({ providerId: req.userId }).lean(),
    ]);

    if (user.profilePic) result[0].value = true;

    if (yourAds.length) {
      result[1].value = true;

      for (const ads of yourAds) {
        if (ads.aboutYou) result[2].value = true;
        if (ads.aboutClass) result[3].value = true;
        if (ads.profileVideo) result[4].value = true;
      }
    }

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: result });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const sortRequest = async (req, res) => {
  try {
    const { sortOrder = -1 } = req.query;
    const usersAd = await Advertisements.find(
      { providerId: req.userId },
      "_id"
    );
    const arrayOfId = usersAd.map((obj) => obj._id);

    const sortedData = await Requests.find({
      adId: { $in: arrayOfId },
      // requestStatus: { $or: [REQUEST_STATUS.PENDING, REQUEST_STATUS.ACCEPT] },
    })
      .populate({
        path: "seekerId adId",
        select: "firstName vedicSubject profilePic",
      })
      .sort({ createdAt: sortOrder });
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: sortedData });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const searchSortFilterProvidersRequest = async (req, res) => {
  try {
    const { searchOption, sortByDate = -1, requestStatus } = req.query;

    const [providersAd, provider] = await Promise.all([
      Advertisements.find({ providerId: req.userId }).distinct("_id"),
      Providers.findById(req.userId, { isPremium: 1 }).lean(),
    ]);

    const totalRequests = await Requests.countDocuments({
      adId: { $in: providersAd },
    });

    let query = {
      adId: { $in: providersAd },
      // $or: [
      //   { requestStatus: REQUEST_STATUS.PENDING },
      //   { requestStatus: REQUEST_STATUS.ACCEPT },
      // ],
    };

    let nonPremiumLimit = 2;
    if (provider.isPremium) {
      nonPremiumLimit = 1000;

      // Search Parameters
      if (searchOption) {
        const regexSearch = new RegExp(searchOption, "i");

        query.$or = [
          { "adData.vedicSubject": { $regex: regexSearch } },
          { "seekerData.firstName": { $regex: regexSearch } },
        ];
      }

    }
    // Filter Parameters
      if (requestStatus) {
        query.requestStatus = requestStatus;
      }

    // Sort Parameters
    let sortOptions = {};
    if (sortByDate) {
      sortOptions.updatedAt = Number(sortByDate);
    }

    const sentRequests = await Requests.aggregate([
      {
        $lookup: {
          from: "advertisements",
          localField: "adId",
          foreignField: "_id",
          as: "adData",
        },
      },
      {
        $lookup: {
          from: "seekers",
          localField: "seekerId",
          foreignField: "_id",
          as: "seekerData",
        },
      },
      {
        $project: {
          _id: 1,
          seekerId: 1,
          adId: 1,
          requestMessage: 1,
          requestStatus: 1,
          sessionPreference: 1,
          updatedAt: 1,
          adData: {
            vedicSubject: 1,
          },
          seekerData: {
            firstName: 1,
            lastName: 1,
            profilePic: 1,
          },
        },
      },
      {
        $match: query,
      },
      {
        $limit: nonPremiumLimit,
      },
      {
        $sort: sortOptions,
      },
    ]);

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.SUCCESS,
      totalRequests,
      data: sentRequests,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getProvidersReview = async (req, res) => {
  try {
    const [totalReviews, providersReview] = await Promise.all([
      Reviews.count({ providerId: req.userId }),
      Reviews.find({
        providerId: req.userId,
      }).populate({ path: "seekerId", select: "firstName profilePic" }),
    ]);
    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.SUCCESS,
      data: { totalReviews, providersReview },
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const calculateProvidersAvgRating = async (req, res) => {
  try {
    const providerReviews = await Reviews.find({ providerId: req.userId });

    const calculateAverage = (ratings) => {
      if (ratings.length === 0) return 0;
      const sumOfRatings = ratings.reduce(
        (acc, rating) => acc + parseFloat(rating.rating.toString()),
        0
      );
      return Number((sumOfRatings / ratings.length).toFixed(2));
    };

    const providerAvgRating = calculateAverage(providerReviews);
    const providerData = await Providers.findById(req.userId).select(
      "-password"
    );
    providerData.rating = providerAvgRating;
    providerData.save();

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: providerData.rating });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const sendReviewReply = async (req, res) => {
  try {
    const { message, reviewId, heartReact } = req.body;
    const review = await Reviews.findById(reviewId);
    review.replyMessage = message;
    if (heartReact) review.heartReact = heartReact;
    review.save();

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: review });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

//TODO: getProviderExperience, updateProviderExperience
const askFaq = async (req, res) => {
  try {
    const { question, adId } = req.body;
    const advertisement = await Advertisements.exists({ adId });
    if (!advertisement) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Advertisement does not exists." });
    }
    await FAQS.create({ question, adId });
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Questions asked successfully." });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || "Error while asking question." });
  }
};

const getFaq = async (req, res) => {
  try {
    const { adId, pageNo = 1, pageLimit = 10 } = req.query;

    if (!mongoose.Types.ObjectId.isValid(adId)) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Please Enter a Valid adId!" });
    }
    const advertisement = await Advertisements.exists({ _id: adId });

    if (!advertisement) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Advertisement does not exists." });
    }

    const skip = (+pageNo - 1) * pageLimit;
    const faqs = await FAQS.find({ adId })
      .sort({ _id: -1 })
      .skip(skip)
      .limit(+pageLimit)
      .lean();
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Faq's fetched successfully.", data: faqs });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || "Error while getting faq's." });
  }
};

const replyOrUpdateFaq = async (req, res) => {
  try {
    const {
      userId,
      body: { faqId, answer },
    } = req;
    const faq = await FAQS.findById(faqId);
    if (!faq) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Faq does not exists." });
    }
    const advertisement = await Advertisements.findById(faq.adId, {
      providerId: 1,
    }).lean();
    if (!advertisement) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Advertisement does not exists anymore." });
    }
    if (userId.toString() !== advertisement.providerId.toString()) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Unauthorized access." });
    }
    faq.answer = answer;
    await faq.save();

    // createPushNotifications()
    // TODO : cant send faq's reply to user, because seekerId is not included in schema.

    return res.status(RESPONSE_STATUS.SUCCESS).json({ message: "Success." });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || "Error while replying faq's." });
  }
};

const sendHelpAndSupportMessage = async (req, res) => {
  try {
    const { category, message } = req.body;

    const user = await Providers.findById(req.userId).lean();

    const mailData = {
      userName: `${user.firstName} ${user.lastName}`,
      email: user.email,
      category: category,
      message: message,
      role: USER_TYPES.PROVIDER
    };
    const html = helpAndSupportEmailTemplate(mailData);

    sendMail(
      HELP_AND_SUPPORT.HELP_AND_SUPPORT_EMAIL,
      "vedicHom : Help & Support Mail",
      html,
      (err, response) => {
        if (err) return console.log("Error Help & Support Mail api");
        return true;
      }
    );

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: mailData });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

module.exports = {
  // ad apis
  getProviderAdvertisements,
  updateAdvertisementStatus,
  uploadAdvertisementMedia,
  // request apis
  getProviderRequests,
  getRejectedRequests,
  sortRequest,
  searchSortFilterProvidersRequest,
  // account apis
  getProviderAccountDetails,
  updateProviderAccountDetails,
  changePasswordProviderAccount,
  updateProviderProfilePicture,
  uploadProviderDocuments,

  getProviderExpStatus,
  // review apis
  getProvidersReview,
  sendReviewReply,
  calculateProvidersAvgRating,
  askFaq,
  getFaq,
  replyOrUpdateFaq,

  // help & support api
  sendHelpAndSupportMessage,
};
