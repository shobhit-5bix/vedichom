const { RESPONSE_STATUS, RESPONSE_MESSAGES } = require("../constants");
const Notification = require("../models/notifications");
const redisCache = require("../auth/redisClient.js");
const Seekers = require("../models/seeker.js");
const Providers = require("../models/providers.js");

const getNotifications = async (req, res) => {
  try {
    const {
      userId,
      query: { pageNo = 1, pageLimit = 10 },
    } = req;
    const page = +pageNo;
    const limit = +pageLimit;
    const skip = (page - 1) * limit;

    const [unSeenCount, totalCount, unSeenNotifications] = await Promise.all([
      Notification.countDocuments({ userId, isClosed: false, isSeen: false }),
      Notification.countDocuments({ userId, isClosed: false }),
      Notification.find(
        { userId, isSeen: false, isClosed: false },
        { title: 1 }
      )
        .sort({ createdAt: -1 })
        .skip(skip)
        .limit(limit)
        .lean(),
    ]);

    const seenNotificationLimit = Math.abs(unSeenNotifications.length - limit);
    const needToSkipSeenNotification = unSeenNotifications.length;
    const unSeenNotificationIds = unSeenNotifications.map(
      (notification) => notification._id
    );

    const [seenNotifications] = await Promise.all([
      Notification.find({ userId, isClosed: false }, { title: 1 })
        .sort({ createdAt: -1 })
        .skip(needToSkipSeenNotification)
        .limit(seenNotificationLimit)
        .lean(),
      Notification.updateMany(
        { _id: { $in: unSeenNotificationIds } },
        { $set: { isSeen: true } }
      ),
    ]);
    const notifications = unSeenNotifications.concat(seenNotifications);

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: "Notifications fetched successfully.",
      totalCount,
      unSeenCount,
      notifications,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const closeNotification = async (req, res) => {
  try {
    const {
      userId,
      body: { notificationId, closeAll },
    } = req;
    if (closeAll) {
      await Notification.updateMany(
        { userId, isClosed: false },
        { $set: { isClosed: true } }
      );
    } else {
      await Notification.updateOne(
        { userId, isClosed: false, _id: notificationId },
        { $set: { isClosed: true } }
      );
    }
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Notification closed successfully." });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getNotificationFromCache = async (req, res) => {
  try {
    const cacheKey = req.userId.toString();

    console.log(`Cache hit for key : ${cacheKey}`);

    const cachedNotifications = await redisCache.redisClient.hGetAll(cacheKey);

    if (cachedNotifications.length) {
      const result = [];
      for (const key in cachedNotifications) {
        const parsedObject = JSON.parse(cachedNotifications[key]);

        // Extract only the desired fields
        const extractedFields = {
          _id: parsedObject._id,
          title: parsedObject.title,
          ifRequestStatus: parsedObject.ifRequestStatus,
          createdAt: parsedObject.createdAt,
        };

        result.push(extractedFields);
      }

      console.log(result);

      return res
        .status(RESPONSE_STATUS.MODIFIED) // setting status non_modified gives no response.
        .json({ message: "Cache hit", data: result });
    } else {
      // If not cached, fetch notifications from the database

      const {pageNo, pageLimit} = req.query;
      console.log(`Cache miss for key : ${cacheKey}`);

      const notifications = await getNotificationsFromDatabase(req.userId, pageNo, pageLimit);
      console.log(notifications);

      if (notifications.length) {
        notifications.forEach(async (item) => {
          await redisCache.redisClient.hSetNX(
            cacheKey,
            item._id.toString(),
            JSON.stringify(notifications)
          );
        });

        return res
          .status(RESPONSE_STATUS.MODIFIED)
          .json({ message: "Cache miss", data: notifications });
      } else {
        return res
          .status(RESPONSE_STATUS.SUCCESS)
          .json({ message: "No Notifications Found!" });
      }
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const deleteNotificationInCache = async (req, res) => {
  try {
    const cacheKey = req.userId.toString();

    const delCachedNotifications = await redisCache.redisClient.del(cacheKey);

    console.log(`Value deleted for key : ${cacheKey}`);

    return res
      .status(RESPONSE_STATUS.MODIFIED) // setting status non_modified gives no response.
      .json({ message: "Cache cleared!", data: delCachedNotifications });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

// Functions  ======================================================================

const getNotificationsFromDatabase = async (
  userId,
  pageNo = 1,
  pageLimit = 10
) => {
  // const pageNo = 1;
  // const pageLimit = 10;

  const skip = (pageNo - 1) * pageLimit;

  const storeNotificationsToCache = await Notification.find(
    { userId, isSeen: true, isClosed: false },
    { title: 1, ifRequestStatus: 1, createdAt: 1 }
  )
    .sort({ createdAt: -1 })
    .skip(skip)
    .limit(pageLimit)
    .lean();

  return storeNotificationsToCache;
};

const createPushNotifications = async (receiverId, senderId, messageTitle) => {
  try {
    const [receiverData, senderData] = await Promise.all([
      Seekers.findOne(
        { $or: [{ _id: receiverId }, { _id: senderId }] },
        { firstName: 1 }
      ).lean(),
      Providers.findOne(
        { $or: [{ _id: receiverId }, { _id: senderId }] },
        { firstName: 1 }
      ).lean(),
    ]);

    const createdNotification = await Notification.create({
      userId: receiverId,
      title: `${senderData.firstName} ${senderData.lastName} ${messageTitle}`,
    });

    // setting notification in cache.
    redisCache.redisClient.set(
      receiverId.toString(),
      JSON.stringify(createdNotification)
    );

    return true;
  } catch (error) {
    throw new Error("Error in createPushNotifications function");
  }
};

module.exports = {
  getNotifications,
  closeNotification,

  // caching notifications
  getNotificationFromCache,
  deleteNotificationInCache,

  // Functions
  createPushNotifications,
};
