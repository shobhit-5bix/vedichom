const Seekers = require("../models/seeker.js");
const Payment = require("../models/payment.js");

const {
  RESPONSE_STATUS,
  RESPONSE_MESSAGES,
  PAYMENT_STATUS,
} = require("../constants.js");

const Razorpay = require("razorpay");
const crypto = require("crypto");
const Providers = require("../models/providers.js");

const instance = new Razorpay({
  key_id: process.env.RAZORPAY_API_KEY,
  key_secret: process.env.RAZORPAY_API_SECRET,
});

const checkout = async (req, res) => {
  try {
    // amount = req.body.amount;
    const { amount } = req.body;

    const options = {
      amount: Number(amount * 100),
      currency: "INR",
      receipt: "order_rcptid_10",
    };

    // TODO: have to check in both the collections.
    let checkUser = await Seekers.findById(req.userId);
    if(!checkUser){
      checkUser = await Providers.findById(req.userId)
    }
    if (!checkUser.isEmailVerified) {
      return res
        .status(RESPONSE_STATUS.FORBIDDEN)
        .json(RESPONSE_MESSAGES.EMAIL_NOT_VERIFIED);
    }

    const order = await instance.orders.create(options);
    const paymentObject = {
      userId: req.userId,
      amount: options.amount,
      orderId: order.id,
      // paymentId: order.internalId,
      paymentStatus: PAYMENT_STATUS.CREATED,
    };
    const newPayment = await Payment.create(paymentObject);
    // order.internalId = newPayment._id;
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ data: { order, newPayment } });
  } catch (error) {
    console.error(error);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: "Error in checkout api", detail: error.message });
  }
};

const paymentVerifation = async (req, res) => {
  try {
    const { razorpay_payment_id, razorpay_signature } = req.body;
    const { orderId } = req.query;
    const fetchedPayment = await Payment.findOne({
      userId: req.userId,
      orderId: orderId,
      paymentStatus: PAYMENT_STATUS.CREATED,
    });
    if (!fetchedPayment) {
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json({ message: "Payment not found in Payment collection" });
    }
    let user = await Seekers.findById(req.userId);
    if(!user){
      user = await Providers.findById(req.userId);
    }

    const load = fetchedPayment.orderId + "|" + razorpay_payment_id;
    const generated_signature = crypto
      .createHmac("sha256", process.env.RAZORPAY_API_SECRET)
      .update(load.toString())
      .digest("hex");

    if (generated_signature == razorpay_signature) {
      fetchedPayment.paymentStatus = PAYMENT_STATUS.SUCCESS;
      fetchedPayment.paymentId = razorpay_payment_id;
      fetchedPayment.save();
      console.log(
        `Signature matched! : ${generated_signature}, ${razorpay_signature} `
      );
      // changing the premium status of user to true.
      user.isPremium = true;
      user.save();

      // return res.redirect(`${process.env.FEHOST}/dash/payment`);
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Payment Verified Successfully" });
    } else {
      fetchedPayment.paymentStatus = PAYMENT_STATUS.FAILURE;
      fetchedPayment.save();
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json({ message: "Payment verification failed" });
    }
  } catch (error) {
    console.log(error.stack);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: "Error in paymentVerification api" });
  }
};

const getPayment = async (req, res) => {
  try {
    const userPayments = await Payment.find({ userId: req.userId });
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: userPayments });
  } catch (error) {
    console.log(error.stack);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: "Error in fetching payments" });
  }
};

const getKey = async (req, res) => {
  return res
    .status(RESPONSE_STATUS.SUCCESS)
    .json({ key: process.env.RAZORPAY_API_KEY });
};

module.exports = {
  checkout,
  paymentVerifation,
  getPayment,
  getKey,
};
