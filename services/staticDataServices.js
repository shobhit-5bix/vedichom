const Faq = require("../models/faq.js");
const axios = require("axios");
const { RESPONSE_STATUS, RESPONSE_MESSAGES } = require("../constants.js");
const { VEDIC_SUBJECT, LANGUAGES } = require("../staticData.js");

// functions

function getRandomElements(array, n) {
  const shuffled = array.sort(() => 0.5 - Math.random());
  return shuffled.slice(0, n);
}
function searchSubjects(keyword, subjectsArray) {
  const searchRegex = new RegExp(keyword, "i");
  return subjectsArray.filter((subject) => searchRegex.test(subject.value));
}

const vedicSubjectsData = (req, res) => {
  try {
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: VEDIC_SUBJECT });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const vedicSubSuggestion = (req, res) => {
  try {
    const { subject } = req.query;
    if (!subject || subject == (undefined || "" || "undefined")) {
      // TODO : 
      // for now we will show random subjects in place of most booked once.

      const randomSubs = getRandomElements(VEDIC_SUBJECT, 6);
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: RESPONSE_MESSAGES.SUCCESS, data: randomSubs });
    } else {
      const result = searchSubjects(subject, VEDIC_SUBJECT);
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: RESPONSE_MESSAGES.SUCCESS, data: result });
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const languageData = (req, res) => {
  try {
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: LANGUAGES });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getFaqs = async (req, res) => {
  try {
    const { category } = req.query;
    const requiredFaqs = await Faq.find({ category: category });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: requiredFaqs });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getGeoLocation = async (req, res) => {
  try {
    const { lat = 30.7117333, lon = 76.6967939 } = req.query;

    const myAccessToken = "pk.17be47e471f0b5a06f2ea325abbf27e2";

    const response = await axios.get(
      `https://us1.locationiq.com/v1/reverse?key=${myAccessToken}&lat=${lat}&lon=${lon}&format=json`
    );
    const currentLocation = response.data;

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: "Current location fetched successfully!",
      data: currentLocation,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getReverseGeocoding = async (req, res) => {
  try {
    // using mapbox geocoding api
    const { longitude, latitude } = req.query;

    const myAccessToken =
      "pk.eyJ1IjoibGlmZXN0ZWFsZXIiLCJhIjoiY2xyZ2NoYnppMDU1dTJxbXE3aDJhY2ZvYyJ9.X2e00Rb4vU6ueS5cTYtMLA";
    const endpoint = "mapbox.places";

    const response = await axios.get(
      `https://api.mapbox.com/geocoding/v5/${endpoint}/${longitude},${latitude}.json?access_token=${myAccessToken}`
    );
    const currentLocation = response.data;

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: "Current location fetched successfully!",
      data: currentLocation,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getLocationSuggestion = async (req, res) => {
  try {
    // using mapbox search api
    const { search, limit = 10 } = req.query;

    const myAccessToken =
      "pk.eyJ1IjoibGlmZXN0ZWFsZXIiLCJhIjoiY2xyZ2NoYnppMDU1dTJxbXE3aDJhY2ZvYyJ9.X2e00Rb4vU6ueS5cTYtMLA";
    // const session_token = "f55b5533-6088-494b-b04c-94af0692e976"

    const response = await axios.get(
      `https://api.mapbox.com/search/searchbox/v1/suggest?q=${search}language=en&limit=${limit}&session_token=${req.sessionID}&access_token=${myAccessToken}`
    );
    const currentLocation = response.data;

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: "Current location fetched successfully!",
      data: currentLocation,
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

module.exports = {
  vedicSubjectsData,
  vedicSubSuggestion,
  languageData,
  getFaqs,
  getGeoLocation,
  getReverseGeocoding,
  getLocationSuggestion,
};
