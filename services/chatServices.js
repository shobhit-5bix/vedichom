const { initializeApp } = require("firebase/app");
const {
  getFirestore,
  Timestamp,
  FieldValue,
  Filter,
  addDoc,
  collection,
  query,
  where,
  orderBy,
  limit,
  getDocs,
} = require("firebase/firestore");

const firebaseConfig = {
  apiKey:
    process.env.FIREBASE_API_KEY || "AIzaSyDsdEmCLGehA-D8iD46PpmV9uYMbGbwFFg",
  authDomain: "vedichom-chat.firebaseapp.com",
  databaseURL:
    process.env.FIREBASE_FIRESTORE_URL ||
    "https://vedichom-chat-default-rtdb.firebaseio.com",
  projectId: "vedichom-chat",
  storageBucket: "vedichom-chat.appspot.com",
  messagingSenderId: "602450569615",
  appId:
    process.env.FIREBASE_APP_ID || "1:602450569615:web:660c0dce681d4fb8a603fa",
  measurementId: "G-90P95JRR6B",
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getFirestore();

const createFirebaseMessage = async (req, res) => {
  try {
    const { to, text } = req.body;
    const createdDocument = await addDoc(collection(db, "message"), {
      to: to,
      from: req.userId,
      text: text,
      createdAt: new Date(),
    });
    const result = createdDocument.id;
    // console.log("Document created successfully with ID:", documentId);
    return res.status(200).json({
      message: "Data added successfully",
      data: result,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      message: "Error adding data in firestore",
      error: error,
    });
  }
};

const readFirebaseMessage = async (req, res) => {
  try {
    const { senderId, receiverId } = req.query;
    const requiredQuery = await query(
      collection(db, "message"),
      where("from", "==", senderId),
      where("to", "==", receiverId),
      orderBy("createdAt", "desc"),
      limit(100)
    );
    const readDocuments = await getDocs(requiredQuery);
    const result = [];
    readDocuments.forEach((doc) => {
      result.push(doc.data());
    });
    return res.status(200).json({
      message: "Data read successfully",
      data: result,
    });
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      message: "Error reading data from database",
      error: error,
    });
  }
};

module.exports = {
  createFirebaseMessage,
  readFirebaseMessage,
};
