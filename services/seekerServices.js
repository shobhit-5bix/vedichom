const Seekers = require("../models/seeker.js");
const Advertisements = require("../models/advertisements.js");
const Request = require("../models/request.js");
const Reviews = require("../models/review.js");
const bcrypt = require("bcryptjs");
const sharp = require("sharp");
const { generateToken, logoutToken } = require("../auth/authUtils");
const redisCache = require("../auth/redisClient.js");
const {
  forgetPasswordTemplate,
  registrationSuccessEmailTemplate,
  createRequestSucessEmailTemplate,
  receivedRequestSucessEmailTemplate,
  reportedAdEmailTemplate,
} = require("../emailTemplate.js");
const {
  sendMail,
  isPasswordValid,
  encryptString,
  isEmailValid,
  capitalizedWord,
} = require("../commonFunctions.js");
const { fileUpload } = require("../services/s3Services.js");
const notp = require("notp");
const {
  RESPONSE_STATUS,
  RESPONSE_MESSAGES,
  USER_TYPES,
  STATUS,
  NUMERIC_CONSTANTS,
  IMAGES_MIMETYPE,
  REQUEST_STATUS,
  SOCIAL_LOGINS,
  HELP_AND_SUPPORT,
} = require("../constants");
const Notification = require("../models/notifications.js");

const registerUser = async (req, res) => {
  try {
    const { firstName, lastName, email, password, mobileNo } = req.body;
    if (email) {
      const isValid = isEmailValid(email);
      if (!isValid)
        return res
          .status(RESPONSE_STATUS.BAD_REQUEST)
          .json({ message: "Please Enter a Valid Email." });
    }
    const checkDuplicateCred = await Seekers.findOne({
      email: { $regex: email, $options: "i" },
    });
    if (checkDuplicateCred)
      return res
        .status(RESPONSE_STATUS.CONFLICT)
        .json({ message: RESPONSE_MESSAGES.EMAIL_ALREADY_REGISTERED });
    const hashedPassword = encryptString(password);

    const userObject = {
      firstName: capitalizedWord(firstName),
      lastName: capitalizedWord(lastName),
      email,
      mobileNo,
      password: hashedPassword,
    };

    const registeredUser = await Seekers.create(userObject);
    const getRegisteredUser = await Seekers.findOne(
      { email: { $regex: email, $options: "i" } },
      { password: 0, createdAt: 0, updatedAt: 0 }
    );

    const otpToken = notp.totp.gen(
      process.env.OTP_SECRET + getRegisteredUser._id.toString(),
      {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      }
    );
    const mailData = {
      firstName: firstName,
      otp: otpToken,
      role: USER_TYPES.SEEKER,
    };
    const html = registrationSuccessEmailTemplate(mailData);
    sendMail(
      email,
      "vedicHom : Email verification mail",
      html,
      (err, response) => {
        if (err)
          return res
            .status(RESPONSE_STATUS.SERVER_ERROR)
            .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });

        return res.status(RESPONSE_STATUS.SUCCESS).json({
          message: RESPONSE_MESSAGES.SUCCESS,
          response,
        });
      }
    );

    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.REGISTER_SUCCESS,
      registeredUser: getRegisteredUser,
    });
  } catch (error) {
    const error_body = {
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(req.path, error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const query = {
      email: { $regex: email, $options: "i" },
      status: "Active",
    };
    const user = await Seekers.findOne(query).lean();
    if (!user) {
      return res.status(RESPONSE_STATUS.NOT_FOUND).json({
        message: RESPONSE_MESSAGES.INVALID_CRED,
      });
    }
    if (!bcrypt.compareSync(password, user.password)) {
      return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
        message: RESPONSE_MESSAGES.INVALID_CRED,
      });
    }
    delete user.password;
    const params = {
      userId: user._id,
      email: user.email,
      role: USER_TYPES.SEEKER,
    };
    const token = await generateToken(params);
    delete user.password;
    user.role = USER_TYPES.SEEKER;

    return res.json({
      message: RESPONSE_MESSAGES.LOGIN_SUCCESS,
      token: token,
      user: user,
    });
  } catch (error) {
    const error_body = {
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const logoutUser = async (req, res) => {
  try {
    const status = await logoutToken(req);

    if (status)
      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: RESPONSE_MESSAGES.LOGGED_OUT,
      });
    else
      return res
        .status(RESPONSE_STATUS.SERVER_ERROR)
        .json({ message: "Error logging out" });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const verifyEmail = async (req, res) => {
  try {
    const { email, otp } = req.body;
    const askedUser = await Seekers.findOne(
      {
        email: { $regex: email, $options: "i" },
      },
      { password: 0 }
    );
    if (!askedUser)
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json({ message: "User Not Found" });
    if (askedUser.isEmailVerified == true)
      return res
        .status(RESPONSE_STATUS.CONFLICT)
        .json({ message: "email already verified" });

    const isValid = notp.totp.verify(
      otp,
      process.env.OTP_SECRET + askedUser._id.toString(),
      {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      }
    );
    console.log(isValid);
    if (isValid && isValid.delta == 0) {
      askedUser.isEmailVerified = true;
      await askedUser.save();
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Email Verified Successfully!", data: askedUser });
    } else {
      return res
        .status(RESPONSE_STATUS.UNAUTHORIZED)
        .json({ message: "Incorrect Otp!" });
    }
  } catch (error) {
    console.error(req.path, error);
    return res.status(RESPONSE_STATUS.SERVER_ERROR).json({
      message: `Error on email verification : ${error.message}`,
    });
  }
};

const resendOtp = async (req, res) => {
  try {
    const { email } = req.query;

    const existingUser = await Seekers.findOne({
      email: { $regex: email, $options: "i" },
    }).lean();
    if (!existingUser) {
      return res
        .status(RESPONSE_STATUS.FORBIDDEN)
        .json(RESPONSE_MESSAGES.EMAIL_NOT_REGISTERED);
    }

    const otpToken = notp.totp.gen(
      process.env.OTP_SECRET + existingUser._id.toString(),
      {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      }
    );

    const mailData = {
      firstName: existingUser.firstName,
      otp: otpToken,
      role: USER_TYPES.SEEKER,
    };
    const html = registrationSuccessEmailTemplate(mailData);
    sendMail(
      email,
      "vedicHom : Email/User verification mail",
      html,
      (err, response) => {
        if (err)
          return res
            .status(RESPONSE_STATUS.SERVER_ERROR)
            .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });

        return res.status(RESPONSE_STATUS.SUCCESS).json({
          message: RESPONSE_MESSAGES.SUCCESS,
          response,
        });
      }
    );
  } catch (error) {
    console.error(req.path, error);
    return res.status(RESPONSE_STATUS.SERVER_ERROR).json({
      message: `Error on resend Otp : ${error.message}`,
    });
  }
};

const forgetPassword = async (req, res) => {
  try {
    const { email } = req.body;
    let user = await Seekers.findOne({
      email: { $regex: email, $options: "i" },
    }).select("-password");

    if (!user)
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: RESPONSE_MESSAGES.EMAIL_NOT_REGISTERED });
    user.resetPassword = false;
    user.save();
    req.body.userId = user._id;
    const otpToken = notp.totp.gen(user._id.toString(), {
      time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
    });

    const mailData = {
      firstName: user.firstName,
      otp: otpToken,
      role: USER_TYPES.SEEKER,
    };

    const html = forgetPasswordTemplate(mailData);
    sendMail(
      user.email,
      "vedicHom: Password Reset Request",
      html,
      (err, response) => {
        if (err)
          return res
            .status(RESPONSE_STATUS.SERVER_ERROR)
            .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
        return res.json({
          message: RESPONSE_MESSAGES.SUCCESS,
          response,
          email: user.email,
          userId: user._id,
        });
      }
    );
  } catch (forgetPwError) {
    const error_body = {
      error_message: "Error on forget password",
      error_detail:
        typeof forgetPwError == "object"
          ? JSON.stringify(forgetPwError)
          : forgetPwError,
      error_data: req.body,
      api_path: req.path,
      stack: forgetPwError.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const resetPasswordOtpVerify = async (req, res) => {
  try {
    const { email, otp } = req.body;
    const user = await Seekers.findOne({
      email: { $regex: email, $options: "i" },
    }).lean();

    if (user) {
      const isValid = notp.totp.verify(otp, user._id.toString(), {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      });

      if (isValid && isValid.delta == 0) {
        const params = {
          userId: user._id,
          email: user.email,
          role: USER_TYPES.SEEKER,
        };
        const token = await generateToken(params);
        delete user.password;
        user.role = USER_TYPES.SEEKER;
        return res
          .status(RESPONSE_STATUS.SUCCESS)
          .json({ message: "Otp Verified Successfully!", token });
      } else {
        return res
          .status(RESPONSE_STATUS.UNAUTHORIZED)
          .json({ message: "Incorrect Otp!" });
      }
    } else {
      return res.status(RESPONSE_STATUS.FORBIDDEN).json({
        message:
          RESPONSE_MESSAGES.EMAIL_NOT_VERIFIED +
          " or " +
          RESPONSE_MESSAGES.EMAIL_NOT_REGISTERED,
      });
    }
  } catch (eroor) {
    const error_body = {
      error_message: "Error on Reset Otp Verify!",
      error_detail: typeof eroor == "object" ? JSON.stringify(eroor) : eroor,
      error_data: req.body,
      api_path: req.path,
      stack: eroor.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const resetPassword = async (req, res) => {
  try {
    const { password } = req.body;
    const user = await Seekers.findById(req.userId);

    if (!user) {
      return res.status(RESPONSE_STATUS.NOT_FOUND).json({
        message: "Email Doesn't Exist or Not Defined!",
      });
    }
    const hashedPassword = encryptString(password);

    user.password = hashedPassword;
    user.resetPassword = true;
    await user.save();
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Your password has been reset Successfully!" });
  } catch (error) {
    const error_body = {
      error_message: "Error on Reset Password",
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const changePassword = async (req, res) => {
  try {
    const { password, newPassword } = req.body;
    if (password == newPassword)
      return res
        .status(RESPONSE_STATUS.PRECONDITION_FAILED)
        .json({ message: "New password should not match old password" });
    const askedUser = await Seekers.findOne({ _id: req.userId });
    const checkPass = bcrypt.compareSync(password, askedUser.password);
    if (!checkPass)
      return res.status(RESPONSE_STATUS.NOT_FOUND).json({
        message: RESPONSE_MESSAGES.PASS_MISMATCH,
      });

    askedUser.password = bcrypt.hashSync(newPassword.trim(), 10);
    await askedUser.save();
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Password changed Successfully!" });
  } catch (error) {
    const error_body = {
      error_message: "Error on change password",
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const deleteAccount = async (req, res) => {
  try {
    const deletedAccount = await Seekers.findByIdAndUpdate(req.userId, {
      status: STATUS.DELETED,
    });
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Your Account Has Been Successfully Deleted" });
  } catch (error) {
    const error_body = {
      error_message: "Error on Delete Account",
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const getProfile = async (req, res) => {
  try {
    const askedUser = await Seekers.findOne(
      { _id: req.userId },
      { password: 0, createdAt: 0, update: 0 }
    ).lean();

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Get user profile success", data: askedUser });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

// TODO: have to optimise.

// const searchSortProviders = async (req, res) => {
//   try {
//     const { services, vedicSubject, modeOfClass, sortBy, sortOrder } =
//       req.query;

//     const searchOptions = {
//       status: STATUS.ACTIVE,
//     };
//     const sortOptions = {};

//     if (services)
//       searchOptions.services = {
//         $in: services.split(",").map((service) => new RegExp(service, "i")),
//       };
//     if (vedicSubject)
//       searchOptions.vedicSubject = {
//         $in: vedicSubject.split(",").map((subject) => new RegExp(subject, "i")),
//       };
//     if (modeOfClass)
//       searchOptions.modeOfClass = {
//         $in: modeOfClass.split(",").map((mode) => new RegExp(mode, "i")),
//       };

//     if (sortBy && sortOrder) {
//       const order = sortOrder === "ascending" ? 1 : -1;
//       if (sortBy === "price") {
//         sortOptions["hourlyRate"] = order;
//       }
//     }

//     const askedProviders = await Advertisements.find(searchOptions)
//       .sort(sortOptions)
//       .populate({
//         path: "providerId",
//         select: "firstName lastName rating isCredsVerified isEmailVerified",
//         options: { lean: true },
//       });

//     const favAds = {};
//     if (req.userId) {
//       const user = await Seekers.findById(req.userId, {
//         favouriteAds: 1,
//       }).lean();
//       if (user.favouriteAds) {
//         user.favouriteAds.forEach((adId) => {
//           favAds[adId.toString()] = true;
//         });
//       }
//     }
//     const newProviderData = askedProviders.map((provider) => {
//       const newProvider = { ...provider._doc }; // Convert Mongoose document to a plain object
//       newProvider.providerData = newProvider.providerId; // Add the 'providerData' field
//       delete newProvider.providerId; // Remove the 'providerId' field
//       newProvider.isFavourite = favAds[provider._id.toString()] || false;
//       return newProvider;
//     });
//     if (newProviderData.length) {
//       return res.status(RESPONSE_STATUS.SUCCESS).json({
//         message: "Providers successfully searched",
//         data: newProviderData,
//       });
//     }
//     return res
//       .status(RESPONSE_STATUS.NOT_FOUND)
//       .json({ message: "No Search Result Found" });
//   } catch (error) {
//     console.error(error);
//     return res
//       .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
//       .json({ message: error.message || error.stack });
//   }
// };

const searchSortProviders = async (req, res) => {
  try {
    const { services, vedicSubject, modeOfClass, sortBy, sortOrder } =
      req.query;

    const searchOptions = {
      status: STATUS.ACTIVE,
    };
    const sortOptions = {};

    if (services)
      searchOptions.services = {
        $in: services.split(",").map((service) => new RegExp(service, "i")),
      };
    // if (vedicSubject)
    //   searchOptions.vedicSubject = {
    //     $in: vedicSubject.split(",").map((subject) => new RegExp(subject, "i")),
    //   };
    if (vedicSubject) {
      searchOptions.$or = [
        {
          vedicSubject: {
            $in: vedicSubject
              .split(",")
              .map((subject) => new RegExp(subject, "i")),
          },
        },
        {
          "providerData.firstName": {
            $in: vedicSubject
              .split(",")
              .map((subject) => new RegExp(subject, "i")),
          },
        },
      ];
    }
    if (modeOfClass)
      searchOptions.modeOfClass = {
        $in: modeOfClass.split(",").map((mode) => new RegExp(mode, "i")),
      };

    const pipeline = [
      {
        $lookup: {
          from: "providers",
          localField: "providerId",
          foreignField: "_id",
          as: "providerData",
        },
      },
      {
        $unwind: "$providerData",
      },
      {
        $match: searchOptions,
      },
      // {
      //   $sort: sortOptions || {"hourlyRate": 1},
      // },
      {
        $project: {
          providerId: 0,
          "providerData.email": 0,
          "providerData.mobileNo": 0,
          "providerData.landlineNo": 0,
          "providerData.password": 0,
          "providerData.skypeId": 0,
          "providerData.TelegramId": 0,
          "providerData.yearsOfExp": 0,
          "providerData.certification": 0,
          "providerData.credentials": 0,
          "providerData.skills": 0,
          "providerData.postalAddress": 0,
          "providerData.bankDetails": 0,
          "providerData.responseTime": 0,
          "providerData.isPremium": 0,
          "providerData.status": 0,
          "providerData.profilePic": 0,
          "providerData.socialLogin": 0,
          "providerData.updatedAt": 0,
          "providerData.__v": 0,
        },
      },
    ];

    if (sortBy && sortOrder) {
      const order = sortOrder === "ascending" ? 1 : -1;
      if (sortBy === "price") {
        sortOptions["hourlyRate"] = order;
        pipeline.push({
          $sort: sortOptions,
        });
      }
    }

    const askedProviders = await Advertisements.aggregate(pipeline);

    const favAds = {};
    if (req.userId) {
      const user = await Seekers.findById(req.userId, {
        favouriteAds: 1,
      }).lean();
      if (user.favouriteAds) {
        user.favouriteAds.forEach((adId) => {
          favAds[adId.toString()] = true;
        });
      }
    }
    const newProviderData = askedProviders.map((provider) => {
      provider.isFavourite = favAds[provider._id.toString()] || false;
      return provider;
    });
    if (newProviderData.length) {
      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: "Providers successfully searched",
        data: newProviderData,
      });
    }

    // console.log(askedProviders);
    return res
      .status(RESPONSE_STATUS.NOT_FOUND)
      .json({ message: "No Search Result Found", data: newProviderData });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getProviderAdAndReviews = async (req, res) => {
  try {
    const { adId, noOfReviews } = req.query;
    const [adDetail, user] = await Promise.all([
      Advertisements.findById(adId).populate({
        path: "providerId",
        select: "-password",
      }),
      Seekers.findById(req.userId),
    ]);
    if (!adDetail) {
      return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
        message: RESPONSE_MESSAGES.NOT_FOUND,
      });
    }

    // manually changing isFavourite

    if (!(!user || !user.favouriteAds)) {
      adDetail.isFavourite =
        user.favouriteAds.includes(adDetail._id.toString()) || false;
    }

    const [countReviews, providerReviews] = await Promise.all([
      Reviews.count({ providerId: adDetail.providerId }),
      Reviews.find({
        providerId: adDetail.providerId,
      })
        .populate({
          path: "seekerId",
          select: "firstName lastName skills isPremium",
        })
        .select("rating message")
        .limit(noOfReviews || 5)
        .lean(),
    ]);
    return res.status(RESPONSE_STATUS.SUCCESS).json({
      message: RESPONSE_MESSAGES.SUCCESS,
      data: {
        adDetail,
        countReviews,
        providerReviews: providerReviews || "No Reviews Yet",
      },
    });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const makeAdFavourite = async (req, res) => {
  try {
    const { adId } = req.query;
    const user = await Seekers.findById(req.userId, { favouriteAds: 1 });
    if (!user) {
      return res.status(RESPONSE_STATUS.NOT_FOUND).json({
        message: "User not found!",
      });
    }

    const adIndex = user.favouriteAds.indexOf(adId);
    if (adIndex === -1) {
      user.favouriteAds.push(adId);
      await user.save();

      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: "Advertisement Successfully Added to Favorites!",
      });
    } else {
      user.favouriteAds.splice(adIndex, 1);
      await user.save();

      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: "Advertisement Removed from Favorites!",
      });
    }
  } catch (error) {
    console.error(error);
    return res.status(RESPONSE_STATUS.SERVER_ERROR).json({
      message: "Internal Server Error",
    });
  }
};

const getFavAds = async (req, res) => {
  try {
    const user = await Seekers.findById(req.userId, {
      favouriteAds: 1,
    }).lean();
    if (!user.favouriteAds) {
      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: "Favourite Advertisement List is Empty!",
      });
    }

    const requiredAds = await Advertisements.find({
      _id: { $in: user.favouriteAds },
    }).populate({
      path: "providerId",
      select: "firstName lastName rating isCredsVerified isEmailVerified",
      options: { lean: true },
    });

    requiredAds.forEach((element) => {
      element.isFavourite = true;
    });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: requiredAds });
  } catch (error) {
    console.error(error);
    return res.status(error.status || RESPONSE_STATUS.SERVER_ERROR).json({
      message: `Error in getFavAds api: ${error.message}` || error.stack,
    });
  }
};

const updateSeekerAccountDetails = async (req, res) => {
  try {
    const { file } = req;

    let uploadedProfilePic;
    if (file) {
      if (file.size > NUMERIC_CONSTANTS.IMAGE_SIZE) {
        return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
          message: "Room images size should be less than 3MB.",
        });
      }
      if (!IMAGES_MIMETYPE.includes(file.mimetype)) {
        return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
          message:
            "Room images should be in JPEG, PNG, JPG, x-png or bmp format.",
        });
      }
      const resizedbuffer = await sharp(file.buffer)
        .resize({
          width: 250,
          height: 250,
          fit: "inside",
        })
        .toBuffer();

      //saving uploaded image url to aws s3
      uploadedProfilePic = await fileUpload(
        "vedicHom/profilePic",
        `${req.userId}${file.originalname}`,
        resizedbuffer
      );
    }

    const updateAccountOptions = { ...req.body };
    if (req.body.imageUrl) {
      updateAccountOptions.profilePic = req.body.imageUrl;
    } else {
      if (file) {
        updateAccountOptions.profilePic = uploadedProfilePic;
      }
    }

    const user = await Seekers.findOne({ _id: req.userId }).select("-password");
    Object.assign(user, updateAccountOptions);

    user.save();
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: user });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const createRequest = async (req, res) => {
  try {
    const { adId, sessionPreference, date, sessionForWhom, requestMessage } =
      req.body;

    const advertisement = await Advertisements.findById(adId, {
      providerId: 1,
      status: 1,
    });
    if (!advertisement || advertisement.status === STATUS.DELETED) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Advertisement Does not Exists!" });
    }
    const requestOptions = {
      seekerId: req.userId,
      adId: adId,
      sessionPreference,
      date,
      sessionForWhom,
      requestMessage,
    };
    // TODO: this api need to be optimized

    // request creation for premium user

    let [usersRequest, premiumUser] = await Promise.all([
      Request.findOne({
        adId: adId,
        seekerId: req.userId,
      }),
      Seekers.findById(req.userId, {
        firstName: 1,
        lastName: 1,
        isPremium: 1,
        email: 1,
      }).lean(),
    ]);

    if (!usersRequest) {
      if (premiumUser.isPremium) {
        requestOptions.requestStatus = REQUEST_STATUS.ACCEPT;
      }
      const [createdRequest, createdNotification] = await Promise.all([
        Request.create(requestOptions),
        Notification.create({
          userId: advertisement.providerId,
          title: `${premiumUser.firstName} ${premiumUser.lastName} sent you a request.`,

          // TBD : below field not in schema
          ifRequestStatus:
            requestOptions.requestStatus || REQUEST_STATUS.PENDING,
        }),
      ]);

      // console.log("hitting create request ======= ", createdNotification.toString())

      // setting notification in cache - for provider.
      redisCache.redisClient.hSetNX(
        advertisement.providerId.toString(),
        createdNotification._id.toString(),
        JSON.stringify(createdNotification)
      );

      const result = await Request.findOne(
        { seekerId: req.userId, adId: adId },
        { password: 0 }
      )
        .populate({
          path: "adId",
          select: "providerId",
          populate: { path: "providerId", select: "firstName email" },
        })
        .exec();

      const mailData = {
        seekerFirstName: premiumUser.firstName,
        providerFirstName: result.adId.providerId.firstName,
        role: USER_TYPES.SEEKER,
      };
      const seekerHtml = createRequestSucessEmailTemplate(mailData);
      const providerHtml = receivedRequestSucessEmailTemplate(mailData);

      sendMail(
        premiumUser.email,
        "vedicHom : Create Request Mail",
        seekerHtml,
        (err, response) => {
          if (err) return console.log("Error in Create Request Mail api");
          return true;
        }
      );
      sendMail(
        result.adId.providerId.email,
        "vedicHom : Received Request Mail",
        providerHtml,
        (err, response) => {
          if (err) return console.log("Error in Received Request Mail api");
          return true;
        }
      );

      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Request sent", data: result });
    } else {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Request is already created", data: usersRequest });
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const deleteRequest = async (req, res) => {
  try {
    const { requestId } = req.body;
    const user = await Request.findById(requestId);
    if (!user) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "No such Request Found!" });
    } else {
      await Request.findByIdAndDelete(requestId);
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Request deleted Successfully" });
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

// registration through Google
const registerSeeker = async (req, res) => {
  try {
    const userDetails = req.user._json;
    let user = await Seekers.findOne({
      email: { $regex: userDetails.email, $options: "i" },
      socialLogin: SOCIAL_LOGINS.GOOGLE,
    });
    if (!user) {
      const userObject = {
        firstName: userDetails.given_name,
        lastName: userDetails.family_name,
        email: userDetails.email,
        isEmailVerified: userDetails.email_verified,
        profilePic: userDetails.picture,
        socialLogin: SOCIAL_LOGINS.GOOGLE,
      };
      user = await Seekers.create(userObject);
    }
    delete user.password;
    const params = {
      userId: user._id,
      email: user.email,
      role: USER_TYPES.PROVIDER,
    };
    const token = await generateToken(params);
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "User registered successfully.", token, user });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const reportAdvertisement = async (req, res) => {
  try {
    const { adId, category, message } = req.body;

    const reportedUser = await Advertisements.findById(adId)
      .populate({ path: "providerId", select: "firstName" })
      .lean();
    const mailData = {
      adId: adId,
      provider: reportedUser.providerId.firstName,
      category: category,
      message: message,
      role: USER_TYPES.SEEKER,
    };
    const html = reportedAdEmailTemplate(mailData);

    sendMail(
      HELP_AND_SUPPORT.HELP_AND_SUPPORT_EMAIL,
      "vedicHom : Ad Report Mail",
      html,
      (err, response) => {
        if (err) return console.log("Error in Ad Report Mail api");
        return true;
      }
    );

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: mailData });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

//

module.exports = {
  registerUser,
  loginUser,
  logoutUser,
  verifyEmail,
  resendOtp,
  forgetPassword,
  resetPasswordOtpVerify,
  resetPassword,
  changePassword,
  deleteAccount,
  updateSeekerAccountDetails,
  // provider list apis
  searchSortProviders,
  getProviderAdAndReviews,
  getProfile,
  makeAdFavourite,
  getFavAds,
  // request apis
  createRequest,
  deleteRequest,
  // social register/login
  registerSeeker,

  // extra apis
  reportAdvertisement,
};
