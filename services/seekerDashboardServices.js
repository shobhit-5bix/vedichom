const Seekers = require("../models/seeker.js");
const Provider = require("../models/providers.js");
const Requests = require("../models/request.js");
const Reviews = require("../models/review.js");
const Advertisements = require("../models/advertisements.js");
const { helpAndSupportEmailTemplate } = require("../emailTemplate.js");
const { sendMail } = require("../commonFunctions.js");
const {
  RESPONSE_STATUS,
  RESPONSE_MESSAGES,
  REQUEST_STATUS,
  NUMERIC_CONSTANTS,
  HELP_AND_SUPPORT,
  USER_TYPES,
} = require("../constants.js");
const { default: mongoose } = require("mongoose");
const { fileUpload } = require("./s3Services.js");

//----------------------------------------Dashboard apis----------------------------------------

const getSeekerAccountDetails = async (req, res) => {
  try {
    const account = await Seekers.findOne({ _id: req.userId });
    if (!account) {
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json(RESPONSE_MESSAGES.NOT_FOUND);
    }
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: account });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const uploadSeekersDocuments = async (req, res) => {
  try {
    const { files } = req;

    if (files.certificate) {
      for (const element of files.certificate) {
        if (element.size > NUMERIC_CONSTANTS.DOCUMENT_SIZE) {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Documents size should not be greater than 10MB!",
          });
        }
        if (element.mimetype != "application/pdf") {
          return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
            message: "Only pdf format can be uploaded!",
          });
        }
      }
    }

    if (files.credential) {
      const credential = files.credential[0];
      if (credential.size > NUMERIC_CONSTANTS.DOCUMENT_SIZE) {
        return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
          message: "Documents size should not be greater than 10MB!",
        });
      }
      if (credential.mimetype != "application/pdf") {
        return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
          message: "Only pdf format can be uploaded!",
        });
      }
    }

    let uploadedCerti = [];
    if (files.certificate) {
      uploadedCerti = await Promise.all(
        files.certificate.map(async (element) => {
          return await fileUpload(
            "vedicHom/documents",
            `${req.userId}${element.originalname}`,
            element.buffer
          );
        })
      );
    }

    let uploadedCreds;
    if (files.credential) {
      uploadedCreds = await fileUpload(
        "vedicHom/documents",
        `${req.userId}${files.credential[0].originalname}`,
        files.credential[0].buffer
      );
    }

    const user = await Seekers.findOne({ _id: req.userId }).select(
      "certification credentials firstName"
    );
    if (uploadedCerti.length) user.certification = uploadedCerti;
    if (uploadedCreds) user.credentials = uploadedCreds;

    await user.save();

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: user });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getSeekersRequest = async (req, res) => {
  try {
    const usersRequest = await Requests.find({
      seekerId: req.userId,
      isDeleted: false,
    }).populate({
      path: "adId",
      select: "providerId vedicSubject",
      populate: {
        path: "providerId",
        select: "firstName profilePic",
      },
    });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: usersRequest });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const sortRequest = async (req, res) => {
  try {
    const { sortOrder = -1 } = req.query;

    const usersRequest = await Requests.find({
      seekerId: req.userId,
      isDeleted: false,
    })
      .populate({
        path: "adId",
        select: "providerId vedicSubject",
        populate: {
          path: "providerId",
          select: "firstName profilePic",
        },
      })
      .sort({ createdAt: sortOrder });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: usersRequest });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getSeekersReviews = async (req, res) => {
  try {
    const { reviewId } = req.query;
    let result = {};
    if (reviewId) {
      result = await Reviews.findOne({
        _id: reviewId,
        seekerId: req.userId,
      }).populate({
        path: "providerId",
        select: "firstName skills",
      });
    } else {
      result = await Reviews.find({ seekerId: req.userId }).populate({
        path: "providerId",
        select: "firstName skills",
      });
    }
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: result });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getRatedUnratedReviews = async (req, res) => {
  try {
    const { rated } = req.query;
    let query = {
      seekerId: req.userId,
    };
    if (rated != undefined) {
      query.message = rated == 1 ? { $exists: true } : null;
    }

    const reviews = await Reviews.find(query).populate({
      path: "providerId",
      select: "firstName skills",
    });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: reviews });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const getListOfAcceptedRequests = async (req, res) => {
  try {
    const acceptedRequests = await Requests.find({
      seekerId: req.userId,
      requestStatus: REQUEST_STATUS.ACCEPT,
      isDeleted: false,
    }).populate({
      path: "adId",
      select: "providerId",
      populate: {
        path: "providerId",
        select: "firstName lastName profilePic",
      },
    });

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: acceptedRequests });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const searchSortFilterSeekersRequest = async (req, res) => {
  try {
    const { searchOption, sortByDate = -1, requestStatus } = req.query;

    // Search Parameters
    let query = {
      seekerId: mongoose.Types.ObjectId(req.userId),
      isDeleted: false,
    };

    if (searchOption) {
      const escapedName = searchOption.replace(/[.*+?^${}()|[\]\\]/g, "\\$&");
      const regexSearch = new RegExp(escapedName, "i");

      query.$or = [
        { "adData.vedicSubject": { $regex: regexSearch } },
        { "providerData.firstName": { $regex: regexSearch } },
      ];
    }

    // Sort Parameters
    let sortOptions = {};
    if (sortByDate) {
      sortOptions.updatedAt = Number(sortByDate);
    }

    // Filter Parameters
    if (requestStatus) {
      query.requestStatus = requestStatus;
    }

    const sentRequests = await Requests.aggregate([
      {
        $lookup: {
          from: "advertisements",
          localField: "adId",
          foreignField: "_id",
          as: "adData",
        },
      },
      {
        $lookup: {
          from: "providers",
          localField: "adData.providerId",
          foreignField: "_id",
          as: "providerData",
        },
      },
      {
        $project: {
          _id: 1,
          seekerId: 1,
          requestMessage: 1,
          requestStatus: 1,
          sessionPreference: 1,
          updatedAt: 1,
          isDeleted: 1,
          adData: {
            vedicSubject: 1,
          },
          providerData: {
            firstName: 1,
            lastName: 1,
            profilePic: 1,
          },
        },
      },
      {
        $match: query,
      },
      {
        $sort: sortOptions,
      },
    ]);

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: sentRequests });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const createReview = async (req, res) => {
  try {
    const { providerId, rating, message } = req.body;
    const createReviewOptions = {
      seekerId: req.userId,
      providerId,
      rating,
      message,
    };
    const createdReview = await Reviews.findOne({
      seekerId: req.userId,
      providerId: providerId,
    });
    // TODO: tbd whether to give option to edit reviews or multiple reviews.
    if (createdReview) {
      if (rating) createdReview.rating = rating;
      if (message) createdReview.message = message;
      createdReview.save();
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Review Updated Successfully", data: createdReview });
    } else {
      const result = await Reviews.create(createReviewOptions);
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Review Created Successfully", data: result });
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const sendHelpAndSupportMessage = async (req, res) => {
  try {
    const { category, message } = req.body;

    const user = await Seekers.findById(req.userId).lean();

    const mailData = {
      userName: `${user.firstName} ${user.lastName}`,
      email: user.email,
      category: category,
      message: message,
      role: USER_TYPES.SEEKER,
    };
    const html = helpAndSupportEmailTemplate(mailData);

    sendMail(
      HELP_AND_SUPPORT.HELP_AND_SUPPORT_EMAIL,
      "vedicHom : Help & Support Mail",
      html,
      (err, response) => {
        if (err) return console.log("Error Help & Support Mail api");
        return true;
      }
    );

    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: RESPONSE_MESSAGES.SUCCESS, data: mailData });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

module.exports = {
  // account apis
  getSeekerAccountDetails,
  uploadSeekersDocuments,
  // request apis
  getSeekersRequest,
  sortRequest,
  getListOfAcceptedRequests,
  searchSortFilterSeekersRequest,
  // review apis
  getSeekersReviews,
  createReview,
  getRatedUnratedReviews,

  // help & support api
  sendHelpAndSupportMessage,
};
