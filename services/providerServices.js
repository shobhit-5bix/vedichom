const notp = require("notp");
const bcrypt = require("bcryptjs");
const {
  RESPONSE_STATUS,
  RESPONSE_MESSAGES,
  REQUEST_STATUS,
  USER_TYPES,
  STATUS,
  NUMERIC_CONSTANTS,
  SOCIAL_LOGINS,
} = require("../constants.js");
const Providers = require("../models/providers.js");
const Notification = require("../models/notifications.js");
const Advertisements = require("../models/advertisements.js");
const Request = require("../models/request.js");
const { generateToken, logoutToken } = require("../auth/authUtils.js");
const redisCache = require("../auth/redisClient.js");
const {
  registrationSuccessEmailTemplate,
  forgetPasswordTemplate,
} = require("../emailTemplate.js");
const {
  sendMail,
  isEmailValid,
  capitalizedWord,
  encryptString,
} = require("../commonFunctions.js");

const registerUser = async (req, res) => {
  try {
    const { firstName, lastName, email, password, mobileNo } = req.body;
    if (email) {
      const isValid = isEmailValid(email);
      if (!isValid)
        return res
          .status(RESPONSE_STATUS.BAD_REQUEST)
          .json({ message: "Please Enter a Valid Email." });
    }
    const checkDuplicateCred = await Providers.findOne({
      email: { $regex: email, $options: "i" },
    });
    if (checkDuplicateCred)
      return res
        .status(RESPONSE_STATUS.CONFLICT)
        .json({ message: RESPONSE_MESSAGES.EMAIL_ALREADY_REGISTERED });
    const hashedPassword = bcrypt.hashSync(password, 8);

    const userObject = {
      firstName: capitalizedWord(firstName),
      lastName: capitalizedWord(lastName),
      email,
      password: hashedPassword,
      mobileNo,
    };
    const user = await Providers.create(userObject);
    const getRegisteredUser = await Providers.findOne(
      { email: { $regex: email, $options: "i" } },
      { password: 0, createdAt: 0, updatedAt: 0 }
    );

    const otpToken = notp.totp.gen(
      process.env.OTP_SECRET + getRegisteredUser._id.toString(),
      {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      }
    );
    const mailData = {
      firstName: firstName,
      otp: otpToken,
      role: USER_TYPES.PROVIDER,
    };
    const html = registrationSuccessEmailTemplate(mailData);
    sendMail(
      email,
      "vedicHom : Email verification mail",
      html,
      (err, response) => {
        if (err)
          return res
            .status(RESPONSE_STATUS.SERVER_ERROR)
            .json({ message: "Error in sendmail function" });

        return res.status(RESPONSE_STATUS.SUCCESS).json({
          message: RESPONSE_MESSAGES.SUCCESS,
          response,
        });
      }
    );

    return res.json({
      message: RESPONSE_MESSAGES.REGISTER_SUCCESS,
      status: RESPONSE_MESSAGES.VERIFICATION_PENDING,
      user: getRegisteredUser,
    });
  } catch (error) {
    const error_body = {
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const loginUser = async (req, res) => {
  try {
    const { email, password } = req.body;

    const query = {
      email: { $regex: email, $options: "i" },
      status: "Active",
    };
    const user = await Providers.findOne(query).lean();
    if (!user) {
      return res.status(RESPONSE_STATUS.NOT_FOUND).json({
        message: RESPONSE_MESSAGES.INVALID_CRED,
      });
    }
    if (!bcrypt.compareSync(password, user.password)) {
      return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
        message: RESPONSE_MESSAGES.INVALID_CRED,
      });
    }
    delete user.password;
    const params = {
      userId: user._id,
      email: user.email,
      role: USER_TYPES.PROVIDER,
    };
    const token = await generateToken(params);
    delete user.password;
    user.role = USER_TYPES.PROVIDER;
    return res.json({
      message: RESPONSE_MESSAGES.LOGIN_SUCCESS,
      token: token,
      user: user,
    });
  } catch (error) {
    const error_body = {
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const logoutUser = async (req, res) => {
  try {
    const status = await logoutToken(req);
    if (status)
      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: RESPONSE_MESSAGES.LOGGED_OUT,
      });
    else
      return res
        .status(RESPONSE_STATUS.SERVER_ERROR)
        .json({ message: "Error logging out" });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const verifyEmail = async (req, res) => {
  try {
    const { otp, email } = req.body;
    const askedUser = await Providers.findOne(
      {
        email: { $regex: email, $options: "i" },
      },
      { password: 0 }
    );
    if (!askedUser)
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json({ message: "User Not Found" });
    if (askedUser.isEmailVerified == true)
      return res
        .status(RESPONSE_STATUS.CONFLICT)
        .json({ message: "email already verified" });
    const isValid = notp.totp.verify(
      otp,
      process.env.OTP_SECRET + askedUser._id.toString(),
      {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      }
    );
    if (isValid && isValid.delta == 0) {
      askedUser.isEmailVerified = true;
      await askedUser.save();
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Email Verified Successfully!", data: askedUser });
    } else {
      return res
        .status(RESPONSE_STATUS.UNAUTHORIZED)
        .json({ message: "Incorrect Otp!" });
    }
  } catch (error) {
    console.error(req.path, error);
    return res.status(RESPONSE_STATUS.SERVER_ERROR).json({
      message: `Error on email verification : ${error.message}`,
    });
  }
};

const resendOtp = async (req, res) => {
  try {
    const { email } = req.query;

    const existingUser = await Providers.findOne({
      email: { $regex: email, $options: "i" },
    }).lean();
    if (!existingUser) {
      return res
        .status(RESPONSE_STATUS.FORBIDDEN)
        .json(RESPONSE_MESSAGES.EMAIL_NOT_REGISTERED);
    }

    const otpToken = notp.totp.gen(
      process.env.OTP_SECRET + existingUser._id.toString(),
      {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      }
    );

    const mailData = {
      firstName: existingUser.firstName,
      otp: otpToken,
      role: USER_TYPES.PROVIDER,
    };
    const html = registrationSuccessEmailTemplate(mailData);
    sendMail(
      email,
      "vedicHom : Email verification mail",
      html,
      (err, response) => {
        if (err)
          return res
            .status(RESPONSE_STATUS.SERVER_ERROR)
            .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });

        return res.status(RESPONSE_STATUS.SUCCESS).json({
          message: RESPONSE_MESSAGES.SUCCESS,
          response,
        });
      }
    );
  } catch (error) {
    console.error(req.path, error);
    return res.status(RESPONSE_STATUS.SERVER_ERROR).json({
      message: `Error on resend Otp : ${error.message}`,
    });
  }
};

const forgetPassword = async (req, res) => {
  try {
    const { email } = req.body;
    let user = await Providers.findOne({
      email: { $regex: email, $options: "i" },
    }).select("-password");
    if (!user)
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: RESPONSE_MESSAGES.EMAIL_NOT_REGISTERED });
    user.resetPassword = false;
    user.save();
    req.body.userId = user._id;
    const otpToken = notp.totp.gen(user._id.toString(), {
      time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
    });

    const mailData = {
      firstName: user.firstName,
      otp: otpToken,
      role: USER_TYPES.PROVIDER,
    };
    const html = forgetPasswordTemplate(mailData);
    sendMail(
      user.email,
      "vedicHom: Password Reset Request",
      html,
      (err, response) => {
        if (err)
          return res
            .status(RESPONSE_STATUS.SERVER_ERROR)
            .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
        return res.json({
          message: RESPONSE_MESSAGES.SUCCESS,
          response,
          email: user.email,
          userId: user._id,
        });
      }
    );
  } catch (forgetPwError) {
    const error_body = {
      error_message: "Error on forget password",
      error_detail:
        typeof forgetPwError == "object"
          ? JSON.stringify(forgetPwError)
          : forgetPwError,
      error_data: req.body,
      api_path: req.path,
      stack: forgetPwError.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const resetPasswordOtpVerify = async (req, res) => {
  try {
    const { email, otp } = req.body;
    const user = await Providers.findOne({
      email: { $regex: email, $options: "i" },
    }).lean();

    if (user) {
      const isValid = notp.totp.verify(otp, user._id.toString(), {
        time: NUMERIC_CONSTANTS.OTP_EXPIRY_TIME,
      });
      if (isValid && isValid.delta == 0) {
        const params = {
          userId: user._id,
          email: user.email,
          role: USER_TYPES.PROVIDER,
        };
        const token = await generateToken(params);
        delete user.password;
        user.role = USER_TYPES.PROVIDER;
        return res
          .status(RESPONSE_STATUS.SUCCESS)
          .json({ message: "Otp Verified Successfully!", token });
      } else {
        return res
          .status(RESPONSE_STATUS.UNAUTHORIZED)
          .json({ message: "Incorrect Otp!" });
      }
    } else {
      return res.status(RESPONSE_STATUS.FORBIDDEN).json({
        message:
          RESPONSE_MESSAGES.EMAIL_NOT_VERIFIED +
          " or " +
          RESPONSE_MESSAGES.EMAIL_NOT_REGISTERED,
      });
    }
  } catch (eroor) {
    const error_body = {
      error_message: "Error on Reset Password Otp Verify!",
      error_detail: typeof eroor == "object" ? JSON.stringify(eroor) : eroor,
      error_data: req.body,
      api_path: req.path,
      stack: eroor.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const resetPassword = async (req, res) => {
  try {
    const { password } = req.body;
    const user = await Providers.findById(req.userId);

    if (!user) {
      return res.status(RESPONSE_STATUS.NOT_FOUND).json({
        message: "Email Doesn't Exist or Not Defined!",
      });
    }
    const hashedPassword = encryptString(password);

    user.password = hashedPassword;
    user.resetPassword = true;
    await user.save();
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Your password has been reset Successfully!" });
  } catch (error) {
    const error_body = {
      error_message: "Error on Reset Password",
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const deleteAccount = async (req, res) => {
  try {
    const deletedAccount = await Providers.findByIdAndUpdate(req.userId, {
      status: STATUS.DELETED,
    });
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Your Account Has Been Successfully Deleted" });
  } catch (error) {
    const error_body = {
      error_message: "Error on Delete Account",
      error_detail: typeof error == "object" ? JSON.stringify(error) : error,
      error_data: req.body,
      api_path: req.path,
      stack: error.stack,
    };
    console.error(error_body);
    return res
      .status(RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: RESPONSE_MESSAGES.SERVER_ERROR });
  }
};

const createUpdateAd = async (req, res) => {
  try {
    const {
      services,
      vedicSubject,
      skillSet,
      title,
      aboutClass,
      aboutYou,
      phoneNo,
      language,
      location,
      modeOfClass,
      hourlyRate,
      profileImage,
      profileVideo,
      status,
      packages,
      demoVideo,
      youtubeLink,
    } = req.body;
    const { adId } = req.query;

    const onBoardOptions = {
      providerId: req.userId,
      services,
      vedicSubject,
      skillSet,
      title,
      aboutClass,
      aboutYou,
      phoneNo,
      language,
      location,
      modeOfClass,
      hourlyRate,
      profileImage,
      profileVideo,
      status,
      packages,
      demoVideo,
      youtubeLink: youtubeLink?.replace("/watch", "/embeded") ?? undefined,
    };

    const [existingAd, totalAdvertisementsCount, allAdsSubject] =
      await Promise.all([
        Advertisements.findOne({
          _id: adId,
          $or: [{ status: STATUS.ACTIVE }, { status: STATUS.CLOSED }],
        }),
        Advertisements.countDocuments({
          providerId: req.userId,
          $or: [{ status: STATUS.ACTIVE }, { status: STATUS.CLOSED }],
        }),
        Advertisements.find({
          providerId: req.userId,
          status: { $ne: STATUS.DELETED },
        }).distinct("vedicSubject"),
      ]);

    if (allAdsSubject.includes(vedicSubject)) {
      return res.status(RESPONSE_STATUS.BAD_REQUEST).json({
        message: "You already have an Advertisement with this vedicSubject",
      });
    }

    if (existingAd) {
      const updatedAd = await Advertisements.findOneAndUpdate(
        { _id: adId },
        onBoardOptions,
        { new: true, runValidators: true, upsert: false }
      );

      return res.status(RESPONSE_STATUS.SUCCESS).json({
        message: "Advertisement updated Successfully!",
        data: updatedAd,
      });
    } else {
      if (totalAdvertisementsCount >= NUMERIC_CONSTANTS.MAX_ADS_ALLOWED) {
        return res
          .status(RESPONSE_STATUS.BAD_REQUEST)
          .json({ message: "You cannot create more than 3 ads" });
      } else {
        const newAd = await Advertisements.create(onBoardOptions);

        return res
          .status(RESPONSE_STATUS.SUCCESS)
          .json({ message: "Advertisement added Successfully!", data: newAd });
      }
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const acceptOrRejectRequest = async (req, res) => {
  try {
    const { requestId, action } = req.body;
    const requestOptions = {
      _id: requestId,
      requestStatus: REQUEST_STATUS.PENDING,
    };
    const [userReq, providerDetail] = await Promise.all([
      Request.findOne(requestOptions),
      Providers.findById(req.userId).lean(),
    ]);
    if (!userReq) {
      return res
        .status(RESPONSE_STATUS.NOT_FOUND)
        .json({ message: "No Such Request Found!" });
    }

    if (
      action == REQUEST_STATUS.DELETE &&
      userReq.requestStatus == REQUEST_STATUS.PENDING
    ) {
      userReq.requestStatus = REQUEST_STATUS.DELETE;
      userReq.save();
      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Request Deleted Successfully", data: userReq });
    }
    if (
      action == REQUEST_STATUS.ACCEPT &&
      userReq.requestStatus == REQUEST_STATUS.PENDING
    ) {
      //startChat()
      // TODO : start chat with user function called.

      userReq.requestStatus = REQUEST_STATUS.ACCEPT;
      const [saved, createdNotification] = await Promise.all([
        userReq.save(),
        Notification.create({
          userId: userReq.seekerId,
          title: `${providerDetail.firstName} ${providerDetail.lastName} accepted your Request!.`,
        }),
      ]);

      // setting notification in cache - for seeker.
      redisCache.redisClient.set(
        userReq.seekerId.toString(),
        JSON.stringify(createdNotification)
      );

      return res
        .status(RESPONSE_STATUS.SUCCESS)
        .json({ message: "Request Accepted", data: userReq });
    }
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const deleteAdvertisement = async (req, res) => {
  try {
    const {
      body: { adId },
      userId,
    } = req;
    const advertisement = await Advertisements.findById(adId, {
      providerId: 1,
      status: 1,
    });
    if (!advertisement) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Invalid advertisement Id." });
    }
    if (advertisement.status === STATUS.DELETED) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Advertisement already deleted." });
    }
    if (!advertisement.providerId.equals(userId)) {
      return res
        .status(RESPONSE_STATUS.BAD_REQUEST)
        .json({ message: "Unauthorized user." });
    }
    advertisement.status = STATUS.DELETED;
    await advertisement.save();
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "Advertisement deleted successfully." });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

const registerProvider = async (req, res) => {
  try {
    const userDetails = req.user._json;
    let user = await Providers.findOne({
      email: { $regex: userDetails.email, $options: "i" },
      socialLogin: SOCIAL_LOGINS.GOOGLE,
    });
    if (!user) {
      const userObject = {
        firstName: userDetails.given_name,
        lastName: userDetails.family_name,
        email: userDetails.email,
        isEmailVerified: userDetails.email_verified,
        profilePic: userDetails.picture,
        socialLogin: SOCIAL_LOGINS.GOOGLE,
      };
      user = await Providers.create(userObject);
    }
    delete user.password;
    const params = {
      userId: user._id,
      email: user.email,
      role: USER_TYPES.PROVIDER,
    };
    const token = await generateToken(params);
    return res
      .status(RESPONSE_STATUS.SUCCESS)
      .json({ message: "User registered successfully.", token, user });
  } catch (error) {
    console.error(error);
    return res
      .status(error.status || RESPONSE_STATUS.SERVER_ERROR)
      .json({ message: error.message || error.stack });
  }
};

module.exports = {
  registerUser,
  loginUser,
  logoutUser,
  verifyEmail,
  resendOtp,
  forgetPassword,
  resetPasswordOtpVerify,
  resetPassword,
  deleteAccount,
  createUpdateAd,
  acceptOrRejectRequest,
  deleteAdvertisement,
  registerProvider,
};
