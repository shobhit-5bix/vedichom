const mongoose = require("mongoose");

const DATABASE = process.env.DATABASE;

mongoose
    .connect(DATABASE, {
        dbName: "astro",
        useNewUrlParser: true,
        useUnifiedTopology: true,
        keepAlive: true,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        w: "majority",
    })
    .then(() => console.info("Connection successful!"))
    .catch((e) => {
        console.error(`Error connecting to ${DATABASE}`, e);
        throw new Error("Error Occurred!");
    });

process.on('SIGINT', function () {
    mongoose.connection.close(function () {
        console.info('Mongoose disconnected on app termination');
        process.exit(0);
    });
});

mongoose.Promise = require("bluebird");
