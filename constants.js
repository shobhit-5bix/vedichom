exports.RESPONSE_STATUS = {
  GONE: 410,
  CREATED: 201,
  SUCCESS: 200,
  BAD_REQUEST: 400,
  SERVER_ERROR: 500,
  BAD_GATEWAY: 502,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  CONFLICT: 409,
  PRECONDITION_FAILED: 412,
  MODIFIED: 302,
  NOT_MODIFIED: 304,
};

exports.RESPONSE_MESSAGES = {
  REGISTER_SUCCESS: "Registered Successfully",
  VERIFICATION_PENDING: "Verification by team pending",
  LINK_EXPIRED: "Link Expired",
  USER_NAME_OR_PASSWORD_IS_INCORRECT: "Username or password is incorrect",
  SUCCESS: "Request Successful",
  OTP_VERIFIED: "Otp verify successfully",
  USER_REGISTERED_SUCCESS: "User registered success",
  LOGIN_SUCCESS: "Login success",
  OTP_EXPIRED: "Otp expired",
  ALREADY_REGISTERED: "User already registered",
  INVALID_OTP: "Invalid Otp",
  USER_NOT_VERIFIED_OR_DOES_NOT_EXISTS: "User not verfied or does not exists",
  USER_NOT_VERIFIED: "User not verfied",
  CONFLICT: "Request Conflicted",
  PASS_MISMATCH: "Password did not match",
  SERVER_ERROR: "Server error occured",
  NOT_VERIFIED: "User not verified",
  NOT_FOUND: "Resource not found",
  TOKEN_NOT_FOUND: "No token provided.",
  TOKEN_SESSION: "User session expired, Please log in again!",
  PARAMETER_NOT_FOUND: "Parameter Missing",
  INVALID_CRED: "Invalid credentials",
  UNAUTHORIZED: "Request Unauthorized",
  PHONE_ALREADY_REGISTERED: "This Phone Number is already registered",
  PHONE_NOT_REGISTERED: "This Phone Number is not registered",
  USERNAME_REGISTERED: "This username is already registered",
  PAN_ALREADY_REGISTERED: "This PAN Number is already registered",
  EMAIL_ALREADY_REGISTERED: "Email Already Registered",
  EMAIL_NOT_REGISTERED: "Email Not Registered",
  EMAIL_SENT: "check updated email",
  BLOCKED_USER: "This user is inactive, Please contact to admin",
  INVALID_IMAGE:
    "Uploaded file is not a valid image. Only JPEG, JPG and PNG files are allowed",
  EVENT_IS_NOT_ACTIVE: "Event is not active now ",
  INVALID_FILE:
    "Uploaded file is not a valid type. Only docx, pdf and doc files are allowed",
  LOGGED_OUT: "User logged out",
  EMAIL_NOT_VERIFIED: "Email not verified"
};

exports.USER_TYPES = {
  SEEKER: "Seeker",
  PROVIDER: "Provider",
  ADMIN: "Admin",
};

exports.GENDER_TYPES = {
  MALE: "Male",
  FEMALE: "Female",
  OTHER: "Other",
};

exports.STATUS = {
  ACTIVE: "Active",
  DELETED: "Deleted",
  CLOSED: "Closed",
  PENDING: "Pending",
};

exports.SERVICES_TYPES = [
  "Guides",
  "Source",
  "Mentors",
  "Vedic Gurus",
  "Teachers",
];
exports.SEEKERS_INTEREST = [
  "Meditation & Yoga",
  "Astrology",
  "Wellness & Ayurveda",
  "Vedic Pshychology",
  "Vedic Art & Music",
  "Vedic Rituals",
  "Vedic Ethics & Values",
  "Vedic Science",
];
exports.EXPERIENCE = ["Beginner", "Intermediate", "Advanced"];
exports.SESSION_PREFERENCE = {
  VIRTUAL: "Virtual",
  REAL_TIME: "Real Time",
  ANY: "Any",
};
exports.LESSON_PREFERENCE = ["Private", "Group", "Any"];

exports.CANCELED_JOBS = ["Res Cancel", "Comm Cancel"];

exports.REQUEST_STATUS = {
  ACCEPT: "Accept",
  DELETE: "Delete",
  PENDING: "Pending",
};
exports.SESSION_FOR_WHOM = {
  MYSELF: "Myself",
  SOMEONE_ELSE: "Someone Else",
};

exports.RESPONSE_TIME = [
  "Within a Hour",
  "6 Hours",
  "Within a Day",
  "3 Days",
  "Within a Week",
  "2 Weeks",
  "Within a Month",
];

exports.PAYMENT_STATUS = {
  CREATED: "CREATED",
  PROCESSING: "PROCESSING",
  SUCCESS: "SUCCESS",
  PENDING: "PENDING",
  FAILED: "FAILED",
  REFUNDED: "REFUNDED",
};

exports.NUMERIC_CONSTANTS = {
  PROFILE_PIC_SIZE: 2 * 1024 * 1000,
  DOCUMENT_SIZE: 10 * 1024 * 1000,
  PROFILE_VIDEO_SIZE: 20 * 1024 * 1000,

  MAX_ADS_ALLOWED: 3,
  OTP_EXPIRY_TIME: 60 * 2,
}

exports.IMAGES_MIMETYPE = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/x-png",
  "image/bmp",
]

exports.VIDEO_MIMETYPE = [
  "video/mp4",
  "video/webm",
  "video/ogg",
  "video/mpeg",
  "video/mkv",
  "video/quicktime"
]

exports.SOCIAL_LOGINS = {
  GOOGLE: "GOOGLE",
  FACEBOOK: "FACEBOOK"
}

exports.SOCIAL_LOGIN_REDIRECT_URL = {
  "/api/provider/auth/google": "/api/provider/auth/google/callback",
  "/api/seeker/auth/google": "/api/seeker/auth/google/callback",
}

exports.HELP_AND_SUPPORT = {
  ADMIN_EMAIL: "vedic.dev@gmail.com",
  HELP_AND_SUPPORT_EMAIL: "vedic.dev@gmail.com",
}