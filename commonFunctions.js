const bcrypt = require("bcryptjs");
const nodemailer = require("nodemailer");
const notp = require("notp");


const encryptString = (str) => {
  return bcrypt.hashSync(str, 8);
};

const safeRegex = (regex, allowed_type, options) => {
  // Let's scope this out, please don't replace with arrow function
  let regex_options = "";
  let extra_options = {};
  if (options) {
    if (typeof options === "string") {
      regex_options = options;
    } else if (options.constructor.name == "Object") {
      extra_options = options;
    }
  }

  if (!regex_options) regex_options = extra_options.regex_options || "gi";
  try {
    const allowed_types_regex = {
      alpha: "[a-z ]+",
      alphanumeric: "[A-Za-zÀ-ÖØ-öø-ÿ 0-9]+",
    };
    if (extra_options.special_chars) {
      extra_options.special_chars = [
        ...new Set(extra_options.special_chars),
      ].join("");
      allowed_types_regex[allowed_type] = allowed_types_regex[
        allowed_type
      ].replace("]", `${extra_options.special_chars}]`);
    }

    if (allowed_type in allowed_types_regex) {
      const r = new RegExp(allowed_types_regex[allowed_type], regex_options);
      const matches = regex.match(r);
      let output = "";
      if (matches && matches.length) output = matches.join(" ").trim();
      if (extra_options.special_chars) {
        for (const extra_char of extra_options.special_chars) {
          output = output.replace(
            new RegExp(`[${extra_char}]`, "g"),
            `[${extra_char}]`
          );
        }
      }
      return output;
    }
    throw new Error(
      "Invalid allowed_type, please check for types or in allowed_types_regex."
    );
  } catch (safeRegexError) {
    console.error("Error while making regex =>", safeRegexError);
    throw safeRegexError;
  }
};

const timeCalculator = ({ startTime, endTime }) => {
  if (startTime && endTime) {
    const [startHours, startMinutes] = startTime
      ? startTime?.split(":")?.map((part) => +part)
      : [0, 0];
    const [endHours, endMinutes] = endTime
      ? endTime?.split(":")?.map((part) => +part)
      : [0, 0];

    const startTotalMinutes = 60 * startHours + startMinutes;
    let endTotalMinutes = 60 * endHours + endMinutes;
    if (startTotalMinutes > endTotalMinutes) {
      endTotalMinutes += 60 * 24;
    }
    const totalSpentMinutes = endTotalMinutes - startTotalMinutes;
    const hours = `${Math.floor(totalSpentMinutes / 60)}`.padStart(2, "0");
    const minutes = `${totalSpentMinutes % 60}`.padStart(2, "0");
    return {
      inWords: `${hours}:${minutes}`,
      number: totalSpentMinutes / 60,
      totalSpentMinutes: totalSpentMinutes,
    };
  }
  return {
    inWords: `00:00`,
    number: 0,
    totalSpentMinutes: 0,
  };
};

const dateFormatter = (date) => {
  return `${date.getFullYear()}-${`${+date.getMonth() + 1}`.padStart(
    2,
    "0"
  )}-${`${date.getDate()}`.padStart(2, "0")}`;
};

const getDatesBetween = (startDate, endDate) => {
  const dates = [];
  for (
    let date = new Date(startDate);
    date <= new Date(endDate);
    date.setDate(date.getDate() + 1)
  ) {
    dates.push(dateFormatter(new Date(date)));
  }
  return dates;
};

const sendMail = (email, subject, content, callback) => {
  console.log("=============EMAIL==============");
  const transporter = nodemailer.createTransport({
    host: "smtp.hostinger.com",
    port: 465,
    auth: {
      user: process.env.EMAIL_USER,
      pass: process.env.EMAIL_PASS,
    },
  });
  const mailOptions = {
    from: "info@vedichome.online",
    to: email,
    subject: subject,
    text: "from astro!", // plain text body
    html: content, // html body
  };
  // console.log({ mailOptions });
  transporter.sendMail(mailOptions, function (error, info) {
    if (error) {
      {
        console.error("email failed for ", error);
        return callback(error, null);
      }
    }
    console.info("This mail sent to >> ", info.accepted[0]);
    return callback(null, info.response);
  });
};

const isPasswordValid = (password) => {
  return (
    password.length >= 8 &&
    stringContainUpperCaseLetter(password) &&
    stringContainLowerCaseLetter(password) &&
    stringContainNumber(password) &&
    stringContainSpecialCharacter(password)
  );
};

const isEmailValid = (email) => {
  return /^[\w-]+(\.[\w-]+)*@[A-Za-z0-9]+(\.[A-Za-z0-9]+)*(\.[A-Za-z]{2,})$/.test(email)
}

const capitalizedWord = (word) => {
  return word.charAt(0).toUpperCase() + word.slice(1)
}


module.exports = {
  encryptString: encryptString,
  safeRegex: safeRegex,
  timeCalculator: timeCalculator,
  dateFormatter: dateFormatter,
  getDatesBetween: getDatesBetween,
  sendMail: sendMail,
  isPasswordValid: isPasswordValid,
  isEmailValid: isEmailValid,
  capitalizedWord: capitalizedWord,
};
