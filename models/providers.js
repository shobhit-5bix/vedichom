const mongoose = require("mongoose");
const { PROVIDER_SCHEMA } = require("./Schemas/ProviderSchema");

const Provider = new mongoose.Schema(PROVIDER_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Provider", Provider);