const mongoose = require("mongoose");
const { PAYMENT_SCHEMA } = require("./Schemas/PaymentSchema.js");

const Payment = new mongoose.Schema(PAYMENT_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Payment", Payment);