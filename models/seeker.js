const mongoose = require("mongoose");
const { SEEKER_SCHEMA } = require("./Schemas/SeekerSchema");

const Seekers = new mongoose.Schema(SEEKER_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Seekers", Seekers);