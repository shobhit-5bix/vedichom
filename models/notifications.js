const mongoose = require("mongoose");
const { NOTIFICATION_SCHEMA } = require("./Schemas/Notifications");

const Notification = new mongoose.Schema(NOTIFICATION_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Notification", Notification);