const { default: mongoose } = require("mongoose");

const  questionSchema = new mongoose.Schema({
  question: {
    type: String,
    required: true,
  },
  answer: {
    type: String,
    // required: true,
  },
  adId: { type: mongoose.Types.ObjectId, required: true }
});

exports.FAQ_SCHEMA = questionSchema
// exports.FAQ_SCHEMA = {
//   category: { type: String },
//   questions: [questionSchema]
// };


