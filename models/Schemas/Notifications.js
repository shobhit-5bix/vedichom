const mongoose = require("mongoose");

exports.NOTIFICATION_SCHEMA = {
    userId: { type: mongoose.Types.ObjectId, required: true },
    title: { type: String, required: true },
    // category: { type: String, required: true },
    isSeen: { type: Boolean, default: false },
    isClosed: { type: Boolean, default: false },
    ifRequestStatus: { type: String }
};
