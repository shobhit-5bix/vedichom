const mongoose = require("mongoose");
const { PAYMENT_STATUS, USER_TYPES } = require("../../constants.js");

exports.PAYMENT_SCHEMA = {
  userId: { type: mongoose.Schema.Types.ObjectId },
  role: { type: String, enum: Object.values(USER_TYPES) },
  amount: { type: Number },
  orderId: { type: String },
  paymentId: { type: String },
  paymentStatus: { type: String, enum: Object.values(PAYMENT_STATUS) },
};
