const mongoose = require('mongoose')
const { STATUS, GENDER_TYPES, RESPONSE_TIME, SOCIAL_LOGINS } = require('../../constants.js')

exports.PROVIDER_SCHEMA = {
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String },
  gender: { type: String, enum: Object.values(GENDER_TYPES) },
  dob: { type: Date },
  mobileNo: { type: Number },
  landlineNo: { type: Number },
  password: { type: String },
  isEmailVerified: { type: Boolean, default: false },
  rating: { type: mongoose.Types.Decimal128, default: 0 },
  skypeId: { type: String },
  TelegramId: { type: String },
  yearsOfExp: { type: Number },
  certification: { type: [String] },
  credentials: { type: String },
  isCredsVerified: { type: Boolean, default: false },
  skills: { type: [String] },
  postalAddress: { type: String },
  bankDetails: { type: String },
  responseTime: { type: String, enum: RESPONSE_TIME },
  isPremium: { type: Boolean, default: false },
  status: { type: String, enum: STATUS, default: STATUS.ACTIVE },
  profilePic: { type: String },
  socialLogin: { type: String, enum: Object.values(SOCIAL_LOGINS) }
};
