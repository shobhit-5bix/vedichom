const {
    LOOKING_FOR
  } = require("../../constants.js");
const mongoose = require('mongoose');
  
  exports.REVIEW_SCHEMA = {
    seekerId: { type: mongoose.Types.ObjectId, ref: 'Seekers' },
    providerId: { type: mongoose.Types.ObjectId, ref: 'Provider' },
    adId: { type: mongoose.Types.ObjectId, ref: 'Advertisement' },
    rating: { type: Number },
    message: { type: String },
    heartReact: { type: Boolean, default: false },
    replyMessage: { type: String },
  };
  