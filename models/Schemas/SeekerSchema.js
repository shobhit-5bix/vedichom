const { default: mongoose } = require("mongoose");
const {
  STATUS,
  GENDER_TYPES
} = require("../../constants.js");


exports.SEEKER_SCHEMA = {
  firstName: { type: String },
  lastName: { type: String },
  email: { type: String, required: true },
  password: { type: String },
  isEmailVerified: { type: Boolean, required: true, default: false },
  resetPassword: { type: Boolean, default: false },
  gender: { type: String, enum: Object.values(GENDER_TYPES) },
  dob: { type: Date },
  mobileNo: { type: Number },
  landlineNo: { type: Number },
  skypeId: { type: String },
  TelegramId: { type: String },
  profilePic: { type: String },
  isPremium: { type: Boolean, default: false },
  status: { type: String, enum: Object.values(STATUS), default: STATUS.ACTIVE },
  rating: { type: mongoose.Types.Decimal128, default: 0 },
  certification: { type: [String] },
  credentials: { type: String },
  isCredsVerified: { type: Boolean, default: false },
  postalAddress: { type: String },
  bankDetails: { type: String },
  favouriteAds: {type: [mongoose.Schema.Types.ObjectId]},



  // lookingFor: { type: String, enum: LOOKING_FOR },  //delete
  // interest: { type: String, enum: SEEKERS_INTEREST },  //delete
  // experience: { type: String, enum: EXPERIENCE },  //delete
  // language: { type: String },  //delete
  // sessionPreferance: { type: String, enum: SESSION_PREFERENCE },  //delete
  // lessonPreferance: { type: String, enum: LESSON_PREFERENCE },  //delete
};
