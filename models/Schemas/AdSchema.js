const mongoose = require("mongoose");
const {
  STATUS,
  SESSION_PREFERENCE,
  LESSON_PREFERENCE,
  SERVICES_TYPES,
} = require("../../constants.js");

exports.AD_SCHEMA = {
  providerId: {
    type: mongoose.Types.ObjectId,
    ref: "Provider",
  },
  services: { type: [String], enum: SERVICES_TYPES },
  vedicSubject: { type: String },
  skillSet: {type: [String]},
  title: { type: String },
  aboutClass: { type: String },
  aboutYou: { type: String },
  phoneNo: { type: Number },
  language: { type: [String] },
  location: { type: String },
  modeOfClass: { type: String, enum: Object.values(SESSION_PREFERENCE) },
  lesson: { type: String, enum: Object.values(LESSON_PREFERENCE) }, //Private, group or any
  hourlyRate: { type: Number },
  profileImage: { type: String },
  profileVideo: { type: String },
  status: { type: String, enum: Object.values(STATUS), default: STATUS.ACTIVE },
  packages: {
    packageCharges: {
      classes: {type: Number},
      total: {type: Number},
    },
    webcam : {type: Number},
    travelCost: {type: Number},
    firstClassFree: {type: Boolean, default: true},
    additionalDetails: {type: String},
  },
  demoVideo: {type: String},
  isFavourite: {type: Boolean, default: false},
  youtubeLink: {type: String},
};


