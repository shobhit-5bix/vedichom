const {
  LOOKING_FOR,
  SEEKERS_INTEREST,
  EXPERIENCE,
  SESSION_PREFERENCE,
  LESSON_PREFERENCE,
  REQUEST_STATUS,
  SESSION_FOR_WHOM
} = require("../../constants.js");
const mongoose = require('mongoose');

exports.REQUEST_SCHEMA = {
  seekerId: { type: mongoose.Types.ObjectId, ref: 'Seekers' },
  adId: { type: mongoose.Types.ObjectId, ref: 'Advertisement' },
  sessionPreference: {type: String, enum: SESSION_PREFERENCE},
  date: {type: mongoose.Schema.Types.Mixed },
  sessionForWhom: {type: String, enum: SESSION_FOR_WHOM },
  requestMessage: {type: String},
  isDeleted: {type: Boolean, default: false},     // TODO: remove isDeleted, is useless now.
  requestStatus: {type: String, enum: REQUEST_STATUS, default: REQUEST_STATUS.PENDING}

};
