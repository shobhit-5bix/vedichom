const mongoose = require("mongoose");
const { REQUEST_SCHEMA } = require("./Schemas/requestSchema.js");

const Request = new mongoose.Schema(REQUEST_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Requests", Request);
