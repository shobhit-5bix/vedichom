const mongoose = require("mongoose");
const { AD_SCHEMA } = require("./Schemas/AdSchema.js");

const Advertisement = new mongoose.Schema(AD_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Advertisement", Advertisement);