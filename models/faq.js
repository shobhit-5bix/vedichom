const mongoose = require("mongoose");
const { FAQ_SCHEMA } = require("./Schemas/FaqSchema.js");

const Faq = new mongoose.Schema(FAQ_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Faq", Faq);