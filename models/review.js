const mongoose = require("mongoose");
const { REVIEW_SCHEMA } = require("./Schemas/ReviewSchema.js");

const Reviews = new mongoose.Schema(REVIEW_SCHEMA, {
    minimize: false,
    timestamps: true,
});

module.exports = mongoose.model("Reviews", Reviews);