const express = require("express");
const cors = require("cors");
const app = express();
const helmet = require("helmet");
const logger = require("morgan");
const busboy = require("connect-busboy");
const bodyParser = require("body-parser");

const passport = require('passport');
const session = require('express-session');
app.use(session({ secret: process.env.GOOGLE_CLIENT_SECRET, resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());

const { SOCIAL_LOGIN_REDIRECT_URL } = require("./constants.js");
const { initializeGoogleAuth } = require('./oAuth/google.js');


app.use(bodyParser.json());
global.__root = __dirname + "/";
require("./db");

app.use(logger("dev", {
  skip: function (req, res) { return res.statusCode < 400 }
}));

if (process.env.NODE_ENV) {
  superUserCreator();
  timezonesCreator().then(() => { }).catch(err => console.error(err));
}

if (process.env.NODE_ENV !== "production") {
  const corsOptions = {}; // exposedHeaders: "DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type" };
  app.use(cors(corsOptions));
  app.options("*", cors());
}

app.use(express.json());

app.use(
  busboy({
    highWaterMark: 2 * 1024 * 1024, // Set 2MiB buffer
  })
);

app.use(express.urlencoded({ extended: true, limit: "5mb" }));
app.use(express.json({ limit: "5mb" }));
app.use(express.static('uploads'))

app.enable("trust proxy");
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'"],
    },
  })
);

app.use(helmet());

app.use((req, res, next) => {
  if (req.path === "/api/provider/auth/google" || req.path === "/api/seeker/auth/google") {
    const redirectUrl = SOCIAL_LOGIN_REDIRECT_URL[req.path]
    initializeGoogleAuth(redirectUrl)
  }
  return next();
});

const ProviderController = require("./controllers/providerController.js");
app.use("/api/provider", ProviderController);
const seekerController = require("./controllers/seekerController.js");
app.use("/api/seeker", seekerController);
const staticDataController = require("./controllers/staticDataController.js");
app.use("/api/static-data", staticDataController);
const notificationController = require("./controllers/notificationController.js");
app.use("/api/notification", notificationController);

app.use((error, req, res, next) => {
  if (error) {
    console.error("Unhandled -", req.path, "\n", error);
    return res.status(500).send("Server Error Occurred!");
  }
  return next();
});

module.exports = app;
